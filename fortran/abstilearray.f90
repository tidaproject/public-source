!! Author : Didem Unat
!! dunat@lbl.gov
!! 
!! Date   : May 2013
!! A _abstilearray_ is a multidimensional array of logical tiles.
!! Modified by: M Nufail Farooqi
!! Nov 2015

module abstilearray_module

  use tile_module
  use region_module
  use tida_error_module
  use global_var_module

  implicit none

  type abstilearray

     integer :: dim = 0        !dimension of the array
     integer :: numtiles=0       !total number of tiles 
     integer :: numregions=0       !total number of regional tiles 
     integer :: numbtiles(MAX_SPACEDIM) = 0       !total number of boundary tiles
     integer :: extent(MAX_SPACEDIM) = 0  !number of tiles in each dimension
     integer :: regionextent(MAX_SPACEDIM) = 0  !number of regions in each dimension
     integer :: tilesizes(MAX_SPACEDIM) = 0 !tilesizes (except the last tiles in the region)
     integer :: regionsizes(MAX_SPACEDIM) = 0 !regionsizes (except the last region)
     type(tile) :: bigtile       !bigtile tile represent the array that is being tiled 
     type(tile), pointer, dimension(:)  :: tiles => Null() ! array of tiles
     type(tile), pointer, dimension(:)  :: regiontiles => Null() ! multidimensional array of regions, supporting currently up to 3 dims
     integer, pointer, dimension(:,:,:) :: tile_idxyz => Null() ! ids of boundary tiles 
     integer, pointer, dimension(:)     :: btiles_id => Null() ! ids of boundary tiles 

  end type abstilearray

  interface tida_abstilearray_build
     module procedure abstilearray_build_i ! scalar
     module procedure abstilearray_build_v ! vector
     module procedure abstilearray_build_ata ! from existing abstilearray 
  end interface

  interface abstilearray_build
     module procedure abstilearray_build_i ! scalar
     module procedure abstilearray_build_v ! vector
     module procedure abstilearray_build_ata ! from existing abstilearray 
  end interface

  interface extent
     module procedure abstilearray_extent
     module procedure abstilearray_extent_d
  end interface

  interface regionextent
     module procedure abstilearray_regionextent
  end interface

  interface dataptr
     module procedure abstilearray_dataptr
  end interface

  interface regiondataptr
     module procedure abstilearray_regiondataptr
  end interface

  interface get_dim
     module procedure abstilearray_dim
  end interface

  interface get_tilesizes
     module procedure abstilearray_get_tilesizes
  end interface

  interface get_regionsizes
     module procedure abstilearray_get_regionsizes
  end interface

  interface empty
     module procedure abstilearray_empty
  end interface

  interface built_q
     module procedure abstilearray_built_q
  end interface

  interface destroy
     module procedure abstilearray_destroy
  end interface

  interface numtiles
     module procedure abstilearray_numtiles
     module procedure abstilearray_numtiles_d
  end interface

  interface numregions
     module procedure abstilearray_numregions
     module procedure abstilearray_numregions_d
  end interface

  interface numbtiles
     module procedure abstilearray_numbtiles
  end interface

  interface get_tile
     module procedure abstilearray_get_tile
     module procedure abstilearray_get_tile_d
  end interface

  interface get_regiontile
     module procedure abstilearray_get_regiontile
  end interface

  interface get_btile_id
     module procedure abstilearray_get_btile_id
  end interface

  interface get_bigtile
     module procedure abstilearray_get_bigtile
  end interface

  interface print
     module procedure abstilearray_print
  end interface


  !! Temporarily expands the lower bound of a tile with a given value
  !! Expansion occurs only on the tiles at the physical boundaries of a box
  !! Does not create a new tile (different than grow)
  interface expand_lwb
     module procedure tile_expand_lwb
     module procedure tile_expand_lwb_v
  end interface

  !! Temporarily expands the upper bound of a tile with a given value
  !! Expansion occurs only on the tiles at the physical boundaries of a box
  !! Does not create a new tile (different than grow)
  interface expand_upb
     module procedure tile_expand_upb
     module procedure tile_expand_upb_v
  end interface

  integer, private :: ALLOCATESTATUS, DEALLOCATESTATUS

contains

  pure function tile_expand_lwb(tl, ng, ata) result(r)

    type (tile), intent(in) :: tl
    type (abstilearray), intent(in) :: ata
    integer, intent(in)     :: ng
    integer, dimension(tl%dim) :: r, ng_v
    
    ng_v = ng 
    r = tile_expand_lwb_v(tl, ng_v, ata)
    
  end function tile_expand_lwb

  pure function tile_expand_lwb_v(tl, ng, ata) result(r)

    type (tile), intent(in) :: tl
    type (abstilearray), intent(in) :: ata
    integer, intent(in), dimension(:) :: ng
    integer, dimension(tl%dim) :: r, rtlo
    type (tile) :: rttl
    integer :: i

    r = Huge(1)
    r(1:tl%dim) = tl%lo(1:tl%dim) 

    rttl = get_regiontile(ata, get_regionid(tl))

    do i = 1, size(ng)
       if(tl%lo(i) == rttl%lo(i)) then
          r(i) = r(i) - ng(i)
       end if
    end do
  end function tile_expand_lwb_v

  function tile_expand_upb(tl, ng, ata) result(r)

    type (tile), intent(in) :: tl
    type (abstilearray), intent(in) :: ata
    integer, intent(in)     :: ng
    integer, dimension(tl%dim) :: r, ng_v

    ng_v = ng 
    r = tile_expand_upb_v(tl, ng_v, ata)

  end function tile_expand_upb

  pure function tile_expand_upb_v(tl, ng, ata) result(r)

    type (tile), intent(in) :: tl
    type (abstilearray), intent(in) :: ata
    integer, intent(in), dimension(:) :: ng
    type (tile) :: rttl
    integer, dimension(tl%dim) :: r
    integer :: i


    r = Huge(1)
    r(1:tl%dim) = tl%hi(1:tl%dim) 

    rttl = get_regiontile(ata, get_regionid(tl))

    do i = 1, size(ng)
       if(tl%hi(i) == rttl%hi(i) ) then
          r(i) = r(i) + ng(i)
       end if
    end do
  end function tile_expand_upb_v

  pure function abstilearray_regionextent(ata) result(r)
    type(abstilearray), intent(in) :: ata
    integer, dimension(ata%dim) :: r

    r = ata%regionextent(1:ata%dim)
  end function abstilearray_regionextent

  pure function abstilearray_extent(ata) result(r)
    type(abstilearray), intent(in) :: ata
    integer, dimension(ata%dim) :: r

    r = ata%extent(1:ata%dim)
  end function abstilearray_extent

  pure function abstilearray_extent_d(ata, d) result(r)
    type(abstilearray), intent(in) :: ata
    integer, intent(in) :: d
    integer :: r

    r = ata%extent(d)
  end function abstilearray_extent_d

  !returns the tile array
  function abstilearray_dataptr(ta) result(r)
    type(abstilearray), intent(in) :: ta
    type(tile), pointer :: r(:)
    r => ta%tiles
  end function abstilearray_dataptr

  !returns the tile array
  function abstilearray_regiondataptr(ta) result(r)
    type(abstilearray), intent(in) :: ta
    type(tile), pointer :: r(:)
    r => ta%regiontiles
  end function abstilearray_regiondataptr

  !returns the number of tiles in ta
  pure function abstilearray_numtiles(ta) result(r)
    type(abstilearray), intent(in) :: ta
    integer :: r
    r = ta%numtiles
  end function abstilearray_numtiles

  !number of tiles in dimension d
  function abstilearray_numtiles_d(ta, d) result(r)
    type(abstilearray), intent(in) :: ta
    integer, intent(in)         :: d
    integer :: r
    if(d > ta% dim .and. d <= 0) call tida_error("abstilearray_numtiles_d: undefined dimension d")
    r = ta%extent(d)
  end function abstilearray_numtiles_d

  !returns the number of regions in ta
  pure function abstilearray_numregions(ta) result(r)
    type(abstilearray), intent(in) :: ta
    integer :: r
    r = ta%numregions
  end function abstilearray_numregions

  !returns the number of btiles in ta
  pure function abstilearray_numbtiles(ta, d) result(r)
    type(abstilearray), intent(in) :: ta
    integer, intent(in) :: d
    integer :: r
    r = ta%numbtiles(d)
  end function abstilearray_numbtiles

  !number of tiles in dimension d
  function abstilearray_numregions_d(ta, d) result(r)
    type(abstilearray), intent(in) :: ta
    integer, intent(in)         :: d
    integer :: r
    if(d > ta% dim .and. d <= 0) call tida_error("abstilearray_numregions_d: undefined dimension d")
    r = ta%regionextent(d)
  end function abstilearray_numregions_d
     
  pure function abstilearray_dim(ta) result(r)
    type(abstilearray), intent(in) :: ta
    integer :: r
    r = ta%dim
  end function abstilearray_dim

  pure function abstilearray_get_tilesizes(ta) result(r)
    type(abstilearray), intent(in) :: ta
    integer :: r(ta%dim)
    r = ta%tilesizes
  end function abstilearray_get_tilesizes

  pure function abstilearray_get_regionsizes(ta) result(r)
    type(abstilearray), intent(in) :: ta
    integer :: r(ta%dim)
    r = ta%regionsizes
  end function abstilearray_get_regionsizes

  pure function abstilearray_get_btile_id(ta, i) result(r)
    type(abstilearray), intent(in) :: ta
    integer, intent(in) :: i
    integer :: r
    r = ta%btiles_id(i)
  end function abstilearray_get_btile_id
  
  pure function abstilearray_empty(ta) result(r)
    logical :: r
    type(abstilearray), intent(in) :: ta
    r = ta%numtiles == 0
  end function abstilearray_empty

  pure function abstilearray_built_q(ta) result(r)
    logical :: r
    type(abstilearray), intent(in) :: ta
    r = ta%dim /= 0
  end function abstilearray_built_q

  ! (scalar)
  function abstilearray_build_i(lo, hi, inregions, tilesize) result(ta)

    type(abstilearray) :: ta
    integer, intent(in) :: tilesize
    integer, intent(in) :: inregions
    integer, intent(in) :: lo(:), hi(:)
    integer :: tsizes(MAX_SPACEDIM), regions(MAX_SPACEDIM)

    regions = inregions
    tsizes = tilesize

    ta = abstilearray_build_v(lo, hi, regions, tsizes)

  end function abstilearray_build_i

  ! (vector)
  function abstilearray_build_v(lo, hi, inregions, intsizes) result(ta)

    use omp_module
    implicit none

    type(abstilearray) :: ta
    integer, intent(in) :: lo(:), hi(:)
    integer, intent(in), dimension(:), optional :: intsizes
    integer, intent(in), dimension(:), optional :: inregions
    integer :: tlo(MAX_SPACEDIM), thi(MAX_SPACEDIM), rlo(MAX_SPACEDIM), rhi(MAX_SPACEDIM)
    integer :: t, rt, lent(MAX_SPACEDIM), i, num_threads
    integer :: ri, rj, rk, ti, tj, tk, d, lasttsizem, rlo3, rlo2, rlo1, tlo3, tlo2, tlo1
    integer :: bti, btj, btk, tbtiles, tpr_d(MAX_SPACEDIM), lintsizes(MAX_SPACEDIM)
    integer :: tsizes(MAX_SPACEDIM), regions(MAX_SPACEDIM) ,rtsizes(MAX_SPACEDIM)
    type(tile)    :: tl
    type(tile)   :: rtl
   
    if (size(lo) /= size(hi)) call tida_error("abstilearray_BUILD: array lo and hi dim mismatch")
    tl = tile_build(lo, hi)
    if ( tl%dim > MAX_SPACEDIM ) call tida_error("abstilearray_INIT: array dim is larger than the supported dim")
    ta%dim = size(lo)

    if (.NOT.present(inregions)) then
!       regions = 1
       regions = ENV_TIDA_REGIONS
    else
       if(size(inregions) /= ta%dim) then       
          if ( size(inregions) > ta%dim) then
             regions = inregions(1:ta%dim)
          else
             regions(1:size(inregions)) = inregions
          end if
       else
          regions = inregions 
       end if
    end if

    if (.NOT.present(intsizes)) then
       lintsizes = ENV_TIDA_TILESIZES

    else
       lintsizes = intsizes
    end if
    tsizes = extent(tl)
    if(size(lintsizes) /= ta%dim) then       
       if ( size(lintsizes) > ta%dim) then
          tsizes = lintsizes(1:ta%dim)
       else
          tsizes(1:size(lintsizes)) = lintsizes
       end if
    else
       tsizes = lintsizes 
    end if

    ta%bigtile = tl
    ta%tilesizes = tsizes

    !if number of regions are greater than the number of threads, adjust it to number of threads
    !$OMP PARALLEL 
    if (omp_get_thread_num() == 0) then 
       num_threads = omp_get_num_threads()
       if ( regions(1)*regions(2)* regions(3) > num_threads) then 
          regions = 1
          regions(3) = num_threads
       end if
    end if
    !$OMP END PARALLEL 

    rtsizes = (hi-lo+1)/regions

    !check if regionsize is smaller than tilesize
    !if smaller, set it to the tilesize
    do i=1, MAX_SPACEDIM
       if( tsizes(i) > rtsizes(i)) then 
          rtsizes(i) = tsizes(i)
       end if
    end do

    ta%regionsizes = rtsizes
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!! Compute and allocate number of tiles and regiontiles
    lent = extent(ta%bigtile)

    ta%regionextent = 1
    ta%regionextent = ceiling( REAL(lent/rtsizes) )
    ta%numregions = ta%regionextent(3) * ta%regionextent(2) * ta%regionextent(1)

    ta%extent = 1
    do d=1, ta%dim
       ta%extent(d) = ceiling( REAL(rtsizes(d)/tsizes(d)) )
       tpr_d(d) = ta%extent(d)
       if (ta%regionextent(d) .eq. lent(d)/rtsizes(d)) then
          ta%extent(d) = ta%extent(d) * ta%regionextent(d)
       else
          ta%extent(d) = ta%extent(d) * (ta%regionextent(d)-1)
          ta%extent(d) = ta%extent(d) + ceiling( REAL(MOD( lent(d),rtsizes(d) )/tsizes(d)) )
       end if       
    end do

    ta%numtiles = ta%extent(3) * ta%extent(2) * ta%extent(1)
    ta%numbtiles(1) = ta%extent(3)*ta%extent(2) 
    ta%numbtiles(2) = ta%extent(1)*ta%extent(3)
    ta%numbtiles(3) = ta%extent(1)*ta%extent(2)
    tbtiles = ta%numbtiles(1)+ta%numbtiles(2)+ta%numbtiles(3)


    allocate(ta%tiles(ta%numtiles), STAT= ALLOCATESTATUS)
    if(ALLOCATESTATUS /= 0) call tida_error("abstilearray_INIT: allocation FAILED")

    allocate(ta%regiontiles(ta%numregions), STAT= ALLOCATESTATUS)
    if(ALLOCATESTATUS /= 0) call tida_error("abstilearray_INIT: allocation FAILED")

    allocate(ta%btiles_id(1:tbtiles), STAT= ALLOCATESTATUS)
    if(ALLOCATESTATUS /= 0) call tida_error("abstilearray_INIT: allocation FAILED")

    allocate(ta%tile_idxyz(1:ta%extent(1),1:ta%extent(2),1:ta%extent(3) ), STAT= ALLOCATESTATUS)
    if(ALLOCATESTATUS /= 0) call tida_error("abstilearray_INIT: allocation FAILED")


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Now assign lo and hi to tiles and regiontiles
    t = 1
    rt = 1
    ri = 1
    rj = 1
    rk = 1
    ti = 1
    tj = 1
    tk = 1
    bti = 1
    btj = ta%extent(2)*ta%extent(3)+1
    btk = btj + ta%extent(1)*ta%extent(3)

    do rlo3=lo(3), hi(3) , rtsizes(3)
       rlo(3)=rlo3
       rj = 1

       do rlo2=lo(2), hi(2) , rtsizes(2)
       rlo(2)=rlo2
       ri = 1
       tj = tpr_d(2)*(rj-1)+1
          do rlo1=lo(1), hi(1), rtsizes(1)
             rlo(1)=rlo1
             ti = tpr_d(1)*(ri-1)+1

             if (rlo(3) + rtsizes(3) <= hi(3)) then
                rhi(3) = rlo(3) + rtsizes(3)-1
             else
                rhi(3) = hi(3)
             end if
             if (rlo(2) + rtsizes(2) <= hi(2)) then
                rhi(2) = rlo(2) + rtsizes(2)-1
             else
                rhi(2) = hi(2)
             end if
             if (rlo(1) + rtsizes(1) <= hi(1)) then
                rhi(1) = rlo(1) + rtsizes(1)-1
             else
                rhi(1) = hi(1)
             end if

             
             rtl = tile_build(rlo,rhi)
             call set_id(rtl, rt)
             call set_id(rtl, ri,rj,rk)
             call set_regionid(rtl, rt)
             ta%regiontiles(rt) = rtl
             tk = tpr_d(3)*(rk-1)+1 
             do tlo3=rlo(3), rhi(3), tsizes(3)
                tlo(3)=tlo3
                tj = tpr_d(2)*(rj-1)+1
                do tlo2=rlo(2), rhi(2), tsizes(2)
                   tlo(2)=tlo2
                   ti = tpr_d(1)*(ri-1)+1
                   do tlo1=rlo(1), rhi(1), tsizes(1)
                      tlo(1)=tlo1

                      if (tlo(1) + tsizes(1) <= rhi(1)) then
                         thi(1) = tlo(1) + tsizes(1)-1
                      else
                         thi(1) = rhi(1)
                      end if
                      if (tlo(2) + tsizes(2) <= rhi(2)) then
                         thi(2) = tlo(2) + tsizes(2)-1
                      else
                         thi(2) = rhi(2)
                      end if
                      if (tlo(3) + tsizes(3) <= rhi(3)) then
                         thi(3) = tlo(3) + tsizes(3)-1
                      else
                         thi(3) = rhi(3)
                      end if

                       tl = tile_build(tlo,thi)
                       call set_id(tl, t)
                       call set_id(tl, ti,tj,tk)
                       ta%tile_idxyz(ti,tj,tk) = t
                       call set_regionid(tl, rt)
                       ta%tiles(t) = tl
                       if ( tlo1.eq.lo(1) ) then
                          ta%btiles_id(bti) = t
                          bti = bti+1
                       end if
                       if ( tlo2.eq.lo(2) ) then
                          ta%btiles_id(btj) = t
                          btj = btj+1
                       end if
                       if ( tlo3.eq.lo(3) ) then
                          ta%btiles_id(btk) = t
                          btk = btk+1
                       end if

                       t = t+1
                       ti = ti+1
                   end do
                   tj = tj+1
                end do
                tk = tk+1
             end do
             rt = rt+1             
             ri = ri+1
          end do
          rj = rj+1
       end do
       rk = rk+1
    end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  end function abstilearray_build_v

  ! (vector)
  function abstilearray_build_ata(inata, intsizes) result(ta)

    use omp_module
    implicit none

    type(abstilearray) :: ta
    type(abstilearray), intent(in) :: inata
    integer, intent(in), dimension(:) :: intsizes
    integer :: tlo(MAX_SPACEDIM), thi(MAX_SPACEDIM), rlo(MAX_SPACEDIM), rhi(MAX_SPACEDIM)
    integer :: t, rt, lent(MAX_SPACEDIM), i
    integer :: ri, rj, rk, ti, tj, tk, d, lasttsizem, rlo3, rlo2, rlo1, tlo3, tlo2, tlo1
    integer :: bti, btj, btk, tbtiles, tpr_d(MAX_SPACEDIM)
    integer :: tsizes(MAX_SPACEDIM) ,rtsizes(MAX_SPACEDIM),lo(MAX_SPACEDIM),hi(MAX_SPACEDIM)
    type(tile)    :: tl
    type(tile)   :: rtl
   
    ta%dim = get_dim(inata)
    tsizes = extent(get_bigtile(inata))
    if(size(intsizes) /= ta%dim) then       
       if ( size(intsizes) > ta%dim) then
          tsizes = intsizes(1:ta%dim)
       else
          tsizes(1:size(intsizes)) = intsizes
       end if
    else
       tsizes = intsizes 
    end if

    ta%bigtile = get_bigtile(inata)
    lo = lwb(ta%bigtile)
    hi = upb(ta%bigtile)
    ta%tilesizes = tsizes
    ta%regionsizes = get_regionsizes(inata)
    rtsizes = ta%regionsizes
    do d=1, ta%dim
       if(tsizes(d) > rtsizes(d))then
          call tida_error("abstilearray_Build: TileSize > RegionSize")
       end if
    end do
    ta%regionextent = regionextent(inata)
    ta%numregions = numregions(inata)
    lent = extent(ta%bigtile)

    ta%extent = 1
    do d=1, ta%dim
       ta%extent(d) = ceiling( REAL(rtsizes(d)/tsizes(d)) )
       tpr_d(d) = ta%extent(d)
       if (ta%regionextent(d) .eq. lent(d)/rtsizes(d)) then
          ta%extent(d) = ta%extent(d) * ta%regionextent(d)
       else
          ta%extent(d) = ta%extent(d) * (ta%regionextent(d)-1)
          ta%extent(d) = ta%extent(d) + ceiling( REAL(MOD( lent(d),rtsizes(d) )/tsizes(d)) )
       end if       
    end do

    ta%numtiles = ta%extent(3) * ta%extent(2) * ta%extent(1)
    ta%numbtiles(1) = ta%extent(3)*ta%extent(2) 
    ta%numbtiles(2) = ta%extent(1)*ta%extent(3)
    ta%numbtiles(3) = ta%extent(1)*ta%extent(2)
    tbtiles = ta%numbtiles(1)+ta%numbtiles(2)+ta%numbtiles(3)


    allocate(ta%tiles(ta%numtiles), STAT= ALLOCATESTATUS)
    if(ALLOCATESTATUS /= 0) call tida_error("abstilearray_INIT: allocation FAILED")

    allocate(ta%regiontiles(ta%numregions), STAT= ALLOCATESTATUS)
    if(ALLOCATESTATUS /= 0) call tida_error("abstilearray_INIT: allocation FAILED")

    allocate(ta%btiles_id(1:tbtiles), STAT= ALLOCATESTATUS)
    if(ALLOCATESTATUS /= 0) call tida_error("abstilearray_INIT: allocation FAILED")

    allocate(ta%tile_idxyz(1:ta%extent(1),1:ta%extent(2),1:ta%extent(3) ), STAT= ALLOCATESTATUS)
    if(ALLOCATESTATUS /= 0) call tida_error("abstilearray_INIT: allocation FAILED")


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!! Now assign lo and hi to tiles and regiontiles
    t = 1
    rt = 1
    ri = 1
    rj = 1
    rk = 1
    ti = 1
    tj = 1
    tk = 1
    bti = 1
    btj = ta%extent(2)*ta%extent(3)+1
    btk = btj + ta%extent(1)*ta%extent(3)

    do rlo3=lo(3), hi(3) , rtsizes(3)
       rlo(3)=rlo3
       rj = 1

       do rlo2=lo(2), hi(2) , rtsizes(2)
       rlo(2)=rlo2
       ri = 1
       tj = tpr_d(2)*(rj-1)+1
          do rlo1=lo(1), hi(1), rtsizes(1)
             rlo(1)=rlo1
             ti = tpr_d(1)*(ri-1)+1

             if (rlo(3) + rtsizes(3) <= hi(3)) then
                rhi(3) = rlo(3) + rtsizes(3)-1
             else
                rhi(3) = hi(3)
             end if
             if (rlo(2) + rtsizes(2) <= hi(2)) then
                rhi(2) = rlo(2) + rtsizes(2)-1
             else
                rhi(2) = hi(2)
             end if
             if (rlo(1) + rtsizes(1) <= hi(1)) then
                rhi(1) = rlo(1) + rtsizes(1)-1
             else
                rhi(1) = hi(1)
             end if

             
             rtl = tile_build(rlo,rhi)
             call set_id(rtl, rt)
             call set_id(rtl, ri,rj,rk)
             call set_regionid(rtl, rt)
             ta%regiontiles(rt) = rtl
             tk = tpr_d(3)*(rk-1)+1 
             do tlo3=rlo(3), rhi(3), tsizes(3)
                tlo(3)=tlo3
                tj = tpr_d(2)*(rj-1)+1
                do tlo2=rlo(2), rhi(2), tsizes(2)
                   tlo(2)=tlo2
                   ti = tpr_d(1)*(ri-1)+1
                   do tlo1=rlo(1), rhi(1), tsizes(1)
                      tlo(1)=tlo1

                      if (tlo(1) + tsizes(1) <= rhi(1)) then
                         thi(1) = tlo(1) + tsizes(1)-1
                      else
                         thi(1) = rhi(1)
                      end if
                      if (tlo(2) + tsizes(2) <= rhi(2)) then
                         thi(2) = tlo(2) + tsizes(2)-1
                      else
                         thi(2) = rhi(2)
                      end if
                      if (tlo(3) + tsizes(3) <= rhi(3)) then
                         thi(3) = tlo(3) + tsizes(3)-1
                      else
                         thi(3) = rhi(3)
                      end if

                       tl = tile_build(tlo,thi)
                       call set_id(tl, t)
                       call set_id(tl, ti,tj,tk)
                       ta%tile_idxyz(ti,tj,tk) = t
                       call set_regionid(tl, rt)
                       ta%tiles(t) = tl
                       if ( tlo1.eq.lo(1) ) then
                          ta%btiles_id(bti) = t
                          bti = bti+1
                       end if
                       if ( tlo2.eq.lo(2) ) then
                          ta%btiles_id(btj) = t
                          btj = btj+1
                       end if
                       if ( tlo3.eq.lo(3) ) then
                          ta%btiles_id(btk) = t
                          btk = btk+1
                       end if

                       t = t+1
                       ti = ti+1
                   end do
                   tj = tj+1
                end do
                tk = tk+1
             end do
             rt = rt+1             
             ri = ri+1
          end do
          rj = rj+1
       end do
       rk = rk+1
    end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  end function abstilearray_build_ata

  ! destroy abstract tile array
  subroutine abstilearray_destroy(ta)
    type(abstilearray), intent(inout) :: ta   
    if ( associated(ta%tiles) ) then
       deallocate(ta%tiles, stat=DEALLOCATESTATUS) 
       if(DEALLOCATESTATUS /= 0) call tida_error("abstilearray_DESTROY: deallocation FAILED")
       ta%tiles => Null()
    end if

    if ( associated(ta%regiontiles) ) then
       deallocate(ta%regiontiles, stat=DEALLOCATESTATUS) 
       if(DEALLOCATESTATUS /= 0) call tida_error("abstilearray_DESTROY: deallocation FAILED")
       ta%regiontiles => Null()
    end if

    if ( associated(ta%tile_idxyz) ) then
       deallocate(ta%tile_idxyz, stat=DEALLOCATESTATUS) 
       if(DEALLOCATESTATUS /= 0) call tida_error("abstilearray_DESTROY: deallocation FAILED")
       ta%tile_idxyz => Null()
    end if

    if ( associated(ta%btiles_id) ) then
       deallocate(ta%btiles_id, stat=DEALLOCATESTATUS) 
       if(DEALLOCATESTATUS /= 0) call tida_error("abstilearray_DESTROY: deallocation FAILED")
       ta%btiles_id => Null()
    end if
    
    call destroy(ta%bigtile)
    ta%extent = 0
    ta%regionextent = 0
    ta%dim = 0
    ta%numtiles = 0
    ta%numregions = 0
    ta%tilesizes = 0
    ta%regionsizes = 0
  end subroutine abstilearray_destroy
  
  function abstilearray_get_bigtile(ata) result(r)
    type(abstilearray), intent(in) :: ata
    type(tile) :: r

    r = ata%bigtile
  end function abstilearray_get_bigtile

  pure function abstilearray_get_tile(ata, index) result(r)
    type(abstilearray), intent(in) :: ata
    integer, intent(in) :: index
    type(tile) :: r

    r = ata%tiles(index)

  end function abstilearray_get_tile

  pure function abstilearray_get_tile_d(ata, i, j, k) result(r)
    type(abstilearray), intent(in) :: ata
    integer, intent(in) :: i, j, k
    type(tile) :: r
    r = ata%tiles( ata%tile_idxyz(i,j,k) )
  end function abstilearray_get_tile_d

  pure function abstilearray_get_regiontile(ata, index) result(r)
    type(abstilearray), intent(in) :: ata
    integer, intent(in) :: index
    type(tile) :: r

    r = ata%regiontiles(index)
  end function abstilearray_get_regiontile

  subroutine abstilearray_print(ata)
    type(abstilearray) , intent(in) :: ata
    integer :: n, lo(ata%dim), hi(ata%dim)
    type(tile) :: tl

    print*, "Numtiles ", numtiles(ata)
    do n = 1, numtiles(ata)

       tl = get_tile(ata, n)
       lo = lwb(tl)
       hi = upb(tl)
       
       print*,'Tile ', get_id(tl), " IDxyz ", get_id(tl, 1), get_id(tl, 2), get_id(tl, 3), &
       ' Lwb ', lo,' Upb ', hi
           
    end do
  end subroutine abstilearray_print

end module abstilearray_module
