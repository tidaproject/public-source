module c_binding_module

 use, intrinsic :: iso_c_binding

  !C bindings
  interface 

     subroutine fparse_str_to_intvec(name, length, regionVal) BIND(C, NAME='parse_str_to_intvec')

       use, intrinsic :: ISO_C_BINDING

       character(C_CHAR) :: name(*)
       integer(C_INT), value :: length
       integer(C_INT) :: regionVal(*)

     end subroutine fparse_str_to_intvec
  end interface
end module c_binding_module
