module tidalib_module

  use tida_error_module
  use c_binding_module

  use, intrinsic :: ISO_C_BINDING

  public :: tida_initialize
  public :: tida_finalize

contains 

  subroutine tida_initialize
    use global_var_module

    !Initialize the environment variables 

    ENV_TIDA_REGIONS = get_region_env()
    ENV_TIDA_TILESIZES = get_tilesizes_env()

  end subroutine tida_initialize

  subroutine tida_finalize

  end subroutine tida_finalize

  function get_region_env() result(regions)

    character (len=255) :: name 
    character (len=255) :: val
    integer :: length, status 
    integer :: regions(3)

    regions = 1
    name = "TIDA_REGIONS"

    !using C get env variable routine 
    !call get_env_var(name, val, length, status)

    !fortran is better because it returns the length and status
    call get_environment_variable(name, val, length, status, .true.)

    if(status .ge. 2) then 
       call tida_error("get_environment_variable failed status = ")
       print*, status
    else if(status .eq. 1) then   !env var does not exist, use default 
       call tida_warn(name, " is not found, using default")
    else if(status .eq. -1 ) then ! variable is truncated 
       call tida_error("env var is truncated ")
       print*, length  
    else if(length .eq. 0 ) then ! env var exists but no value is set, use default
       call tida_warn(name, " is not set, using default")
    else if(status .eq. 0 ) then ! env variable exists
       call fparse_str_to_intvec(trim(val)//char(0), length+1, regions)
    end if
  end function get_region_env

  function get_tilesizes_env() result(tilesizes)

    character (len=255) :: name 
    character (len=255) :: val
    integer :: length, status 
    integer :: tilesizes(3)

    tilesizes = 1
    name = "TIDA_TILESIZES"

    !using C get env variable routine 
    !call get_env_var(name, val, length, status)

    !fortran is better because it returns the length and status
    call get_environment_variable(name, val, length, status, .true.)

    if(status .ge. 2) then 
       call tida_error("get_environment_variable failed status = ")
       print*, status
    else if(status .eq. 1) then   !env var does not exist, use default 
       call tida_warn(name, " is not found, using default")
    else if(status .eq. -1 ) then ! variable is truncated 
       call tida_error("env var is truncated ")
       print*, length  
    else if(length .eq. 0 ) then ! env var exists but no value is set, use default
       call tida_warn(name, " is not set, using default")
    else if(status .eq. 0 ) then ! env variable exists
       call fparse_str_to_intvec(trim(val)//char(0), length+1, tilesizes)
    end if
  end function get_tilesizes_env

end module tidalib_module
