!! Author: M Nufail Farooqi
!!
!! This module is created to define a tile iterator for tilearray
!! Date: Nov 2015

module tileitr_module

  use tile_module
  use abstilearray_module
  use tida_error_module

  implicit none

  type tileitr
     integer :: dim      = 0
     integer :: tid      = -1
     integer :: it       = 0    ! current tile index local to this thread
     integer :: threadnumtiles   = 0
     type(tile), pointer, dimension(:) :: threadtiles
     logical :: built    = .false.
  end type tileitr

  interface tileitr_build
     module procedure  tilearray_tileitr_build
     module procedure  tilearray_tileitr_build_ts
  end interface tileitr_build

  interface btileitr_build
     module procedure  tilearray_btileitr_build
  end interface btileitr_build

  interface tileitr_reset
     module procedure  tileitr_reset
  end interface tileitr_reset

  interface get_tile
     module procedure  tileitr_get_tile
  end interface get_tile

  interface next_tile
     module procedure  tileitr_next_tile
  end interface next_tile

  interface more_tile
     module procedure  tileitr_more_tile
  end interface more_tile

  interface print
     module procedure  tileitr_print
  end interface print

  interface destroy
     module procedure  tileitr_destroy
  end interface destroy

  interface upb
     module procedure tile_upb_ti
  end interface

  interface lwb
     module procedure tile_lwb_ti
  end interface

contains

  pure function tile_lwb_ti(ti) result(r)
    type (tileitr), intent(in) :: ti
    integer :: r(MAX_SPACEDIM)
    type (tile) :: tl
    integer :: dim

    tl = tileitr_get_tile(ti)
    dim = get_dim(tl)
    r(1:dim) = lwb( tl )
  end function tile_lwb_ti

  pure function tile_upb_ti(ti) result(r)
    type (tileitr), intent(in) :: ti
    integer :: r(MAX_SPACEDIM)
    type (tile) :: tl
    integer :: dim

    tl = tileitr_get_tile(ti)
    dim = get_dim(tl)
    r(1:dim) = upb( tl )
  end function tile_upb_ti

  function tilearray_tileitr_build(ata) result(ti)
    use omp_module
    type(tileitr) :: ti
    type(abstilearray), intent(in) :: ata
    integer :: i, tid, nthreads, ntiles, tlidstart

    tid = omp_get_thread_num()
    nthreads = omp_get_num_threads()

    ntiles = numtiles(ata)
    
    ti%tid = tid
    
    if ( built_q(ata) ) then
       ti%dim = get_dim(ata)
       ti%it = 0
       ti%threadnumtiles = floor( REAL(ntiles/nthreads) )
!!!!! Currently work distribution is dependent on tids
!!!!! Limitation : cannot use TileItr if some TileItr is build and used inside some theards
!!!!!              All threads are used currently
       tlidstart = tid * ti%threadnumtiles
       
       if ( tid .eq. nthreads-1 ) then
          ti%threadnumtiles = ti%threadnumtiles + MOD( ntiles, nthreads )
       end if
       
       allocate( ti%threadtiles( ti%threadnumtiles ) )
          
       do i = 1, ti%threadnumtiles
          ti%threadtiles(i) = get_tile(ata, tlidstart + i)
       end do
       
    else
       ti%it = 0
       ti%threadnumtiles = 0
    end if
    
    ti%built = .true.  ! even when ti%threadnumtiles is 0.

  end function tilearray_tileitr_build

  function tilearray_tileitr_build_ts(ata, tilesizes) result(ti)
    type(tileitr) :: ti
    type(abstilearray), intent(in) :: ata
    integer, intent(in) :: tilesizes(3)
    type(abstilearray) :: newata
    integer :: lo(3), hi(3), numregions

    newata = abstilearray_build(ata, tilesizes)
    ti = tilearray_tileitr_build( newata )
    call destroy(newata)
  end function tilearray_tileitr_build_ts

  function tilearray_btileitr_build(ata,d) result(ti)
    use omp_module
    type(tileitr) :: ti
    type(abstilearray), intent(in) :: ata
    integer, intent(in) :: d
    integer :: i, tid, nthreads, ntiles, tlidstart


    tid = omp_get_thread_num()
    nthreads = omp_get_num_threads()

    ntiles = numbtiles(ata,d)

    ti%tid = tid
    
    if ( built_q(ata) ) then
       ti%dim = get_dim(ata)
       ti%it = 0
       ti%threadnumtiles = floor( REAL(ntiles/nthreads) )
!!!!! Currently work distribution is dependent on tids
!!!!! Limitation : cannot use TileItr if some TileItr is build and used inside some theards
!!!!!              All threads are used currently
       tlidstart = tid * ti%threadnumtiles

       if(d .gt. 1) tlidstart = tlidstart + numbtiles(ata,1)
       if(d .gt. 2) tlidstart = tlidstart + numbtiles(ata,2)
       
       if ( tid .eq. nthreads-1 ) then
          ti%threadnumtiles = ti%threadnumtiles + MOD( ntiles, nthreads )
       end if
       
       allocate( ti%threadtiles( ti%threadnumtiles ) )
          
       do i = 1, ti%threadnumtiles
          ti%threadtiles(i) = get_tile(ata, get_btile_id(ata,tlidstart + i) )
       end do
       
    else
       ti%it = 0
       ti%threadnumtiles = 0
    end if
    
    ti%built = .true.  ! even when ti%threadnumtiles is 0.

  end function tilearray_btileitr_build


  subroutine tileitr_reset(ti)
    type(tileitr), intent(inout) :: ti
    ti%it = 0
  end subroutine tileitr_reset

  subroutine tileitr_print(ti)
    type(tileitr), intent(in) :: ti
    integer :: i
    do i=1, ti%threadnumtiles
       print*,ti%threadnumtiles," Tiles in Iterator with Thread ID : ", ti%tid," are ", &
            get_id(ti%threadtiles(i))
    end do
  end subroutine tileitr_print


  function tileitr_next_tile(ti) result(r)
    logical :: r
    type(tileitr), intent(inout) :: ti

    call tida_assert(ti%built, "Tile Iterator is not built")
    ti%it = ti%it + 1
    if (ti%it .le. ti%threadnumtiles) then
       r = .true.
    else
       ti%it = 0
       r = .false.
    end if
  end function tileitr_next_tile

  function tileitr_more_tile(ti) result(r)
    logical :: r
    type(tileitr), intent(inout) :: ti
    call tida_assert(ti%built, "Tile Iterator is not built")

    if (ti%it+1 .le. ti%threadnumtiles) then
       r = .true.
    else
       r = .false.
    end if
  end function tileitr_more_tile

  pure function tileitr_get_tile(ti) result(r)
    type(tile) :: r
    type(tileitr), intent(in) :: ti
    r = ti%threadtiles(ti%it)
  end function tileitr_get_tile

  subroutine tileitr_destroy(ti)
    type(tile) :: r
    type(tileitr), intent(inout) :: ti
    
    if ( associated(ti%threadtiles) ) then
       deallocate(ti%threadtiles)
    endif
    ti%built = .false.
    ti%dim = 0
    ti%it = 0
    ti%threadnumtiles = 0
  end subroutine tileitr_destroy

end module tileitr_module








