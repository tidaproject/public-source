!! Author : Didem Unat
!! dunat@lbl.gov
!! 
!! Date   : May 2013
!! Updates : March 2014
!!
!! Modified by: M Nufail Farooqi
!! Nov 2015
!! Tilearray contains array of regions for a tiled array
!!

module tilearray_module

  use tida_error_module

  use abstilearray_module
  use region_module
  use tile_module

  use tileitr_module

  use tida_const
  use omp_module

  use global_var_module

  implicit none

 
  type tilearray
     integer         :: dim   = 0
     integer         :: ng    = 0
     integer         :: nc    = 1
     integer         :: numregions=0       !total number of tiles 
     integer         :: regionextent(MAX_SPACEDIM) = 0  !number of regions in each dimension
     integer         :: regionsizes(MAX_SPACEDIM) = 0 !regionsizes (except the last region)
     type(tile)      :: bigtile       !bigtile tile represent the array that is being tiled 
     type(region), pointer:: regions(:,:,:)   => Null()
     integer, pointer, dimension(:,:) :: region_xyzids => Null()
  end type tilearray

  interface built_q
     module procedure tilearray_built_q
  end interface

  interface tilearray_build
     module procedure tilearray_build
  end interface

  interface destroy
     module procedure tilearray_destroy
  end interface

!  interface copy
!     module procedure tilearray_copy
!  end interface

  interface dataptr
     module procedure tilearray_dataptr
     module procedure tilearray_dataptr_ti
     module procedure tilearray_dataptr_id3
     module procedure tilearray_dataptr_ijk
  end interface

!  interface get_abstilearray
!     module procedure tilearray_get_abstilearray
!  end interface

  interface get_ptile
     module procedure tilearray_get_ptile
  end interface

  interface get_region
     module procedure tilearray_get_region
     module procedure tilearray_get_region_ti
  end interface

  interface get_bigtile
     module procedure tilearray_get_bigtile
  end interface

  interface get_dim
     module procedure tilearray_get_dim
  end interface

  interface numregions
     module procedure tilearray_numregions
  end interface

  interface nghost
     module procedure tilearray_nghost
  end interface

  interface ncomp
     module procedure tilearray_ncomp
  end interface

!  interface setval 
!     module procedure tilearray_setval
!  end interface


  !Fills the mtile boundaries from the box
  !Assumes box already exchanged the data
  !Depending on the memory layout, this may fill the boundaries 
  !of the interior mtiles
  interface fill_boundary
     module procedure tilearray_fill_boundary
  end interface

  !fills only the tile boundary 
!  interface fill_tileboundary
!     module procedure tilearray_fill_tileboundary
!  end interface


  !since boxlib is not aware of TiDA, we need to copy 
  !mtiles to a box before ghost exchange
!  interface copy_tilesToBox
!     module procedure tilearray_copy_tilesToBox
!  end interface

  !more optimized version of copy_tilesToBox, this only copies 
  !the shared region to the box, box interior is ill-defined. 
!  interface copy_tileSharedRegionToBox
!     module procedure tilearray_copy_tileSharedRegionToBox
!  end interface
    
  !Debug options 
!  interface tida_print
!     module procedure tilearray_print
!  end interface

!  interface tida_sum
!     module procedure tilearray_sum
!  end interface

!  interface countNULLs
!     module procedure tilearray_countNULLs
!  end interface

!  public  :: cpy_d

  integer, private :: ALLOCATESTATUS, DEALLOCATESTATUS

contains


  pure function tilearray_nghost(ta) result(r)
    type(tilearray), intent(in) :: ta
    integer :: r
    r = ta%ng
  end function tilearray_nghost

  pure function tilearray_ncomp(ta) result(r)
    type(tilearray), intent(in) :: ta
    integer :: r
    r = ta%nc
  end function tilearray_ncomp

  pure function tilearray_built_q(ta) result(r)
    logical :: r
    type(tilearray), intent(in) :: ta
    r = ta%dim /= 0
  end function tilearray_built_q

  pure function tilearray_get_dim(ta) result(r)
    type(tilearray), intent(in) :: ta
    integer :: r
    r = ta%dim
  end function tilearray_get_dim

  pure function tilearray_numregions(ta) result(r)
    type(tilearray), intent(in) :: ta
    integer :: r
    r = ta%numregions
  end function tilearray_numregions

  function tilearray_get_region(ta, i,j,k) result(r)
    type(tilearray), intent(in) :: ta
    integer, intent(in) :: i,j,k
    type(region) :: r

    r = ta%regions(i,j,k)    
  end function tilearray_get_region

  function tilearray_get_region_ti(ta, ti) result(r)
    type(tilearray), intent(in) :: ta
    type(tileitr), intent(in) :: ti
    type(region) :: r
    integer :: regionid, i, j, k

    regionid = get_regionid( get_tile(ti) )
    i = ta%region_xyzids(regionid,1)
    j = ta%region_xyzids(regionid,2)
    k = ta%region_xyzids(regionid,3)
    r = ta%regions(i,j,k)    
  end function tilearray_get_region_ti

  function tilearray_get_bigtile(ta) result(r)
    type(tilearray), intent(in) :: ta
    type(tile) :: r

    r = ta%bigtile
  end function tilearray_get_bigtile

  function tilearray_get_ptile(ta, i,j,k) result(r)
    type(tilearray), intent(in) :: ta
    integer, intent(in) :: i,j,k
    type(tile) :: r

    r = get_ptile(ta%regions(i,j,k))    
  end function tilearray_get_ptile

 function tilearray_dataptr_ti(ta, ti) result(r)
    type(tilearray), intent(in) :: ta
    type(tileitr), intent(in) :: ti
    real(dp_t), pointer :: r(:,:,:,:)

    r => tilearray_dataptr(ta, get_tile(ti))
  end function tilearray_dataptr_ti

 function tilearray_dataptr(ta, tl) result(r)
    type(tilearray), intent(in) :: ta
    type(tile), intent(in) :: tl
    integer :: id(3)
    real(dp_t), pointer :: r(:,:,:,:)

    id = ta%region_xyzids(get_regionid(tl),:)
    r => tilearray_dataptr_ijk(ta, id(1), id(2), id(3))
  end function tilearray_dataptr
   
 function tilearray_dataptr_id3(ta, id) result(r)
    type(tilearray), intent(in) :: ta
    integer, intent(in) :: id(3)
    real(dp_t), pointer :: r(:,:,:,:)
     
    r => tilearray_dataptr_ijk(ta, id(1), id(2), id(3))
  end function tilearray_dataptr_id3
 
  function tilearray_dataptr_ijk(ta, i, j, k) result(r)
    type(tilearray), intent(in) :: ta
    integer, intent(in) :: i,j,k
    real(dp_t), pointer :: r(:,:,:,:)

    r => dataptr(ta%regions(i,j,k))
  end function tilearray_dataptr_ijk

  subroutine tilearray_destroy(ta)
    type(tilearray), intent(inout) :: ta
    integer :: n, i, j, k, id(3)

    do k=1, ta%regionextent(3)
       do j=1, ta%regionextent(2)
          do i=1, ta%regionextent(1)
             call destroy(ta%regions(i, j, k))
          end do
       end do
    end do

    if(associated(ta%regions)) then
       deallocate(ta%regions, stat=DEALLOCATESTATUS)
       if(DEALLOCATESTATUS /= 0) call tida_error("MTILEARRAY_DESTROY: deallocation FAILED")
       ta%regions => Null()
    end if

    if(associated(ta%region_xyzids)) then
       deallocate(ta%region_xyzids, stat=DEALLOCATESTATUS)
       if(DEALLOCATESTATUS /= 0) call tida_error("MTILEARRAY_DESTROY: deallocation FAILED")
       ta%region_xyzids => Null()
    end if

    call destroy(ta%bigtile)
    ta%dim = 0
    ta%ng  = 0    
    ta%nc  = 1
    ta%regionextent = 0
    ta%regionsizes = 0
    ta%numregions = 0
  end subroutine tilearray_destroy

  function tilearray_build(ata, nc, ng) result(ta)
    type(tilearray) :: ta
    type(abstilearray),   intent(in )   :: ata
    integer, intent(in), optional :: nc, ng
!    integer :: lo(MAX_SPACEDIM), hi(MAX_SPACEDIM)
    type(tile) :: tl
    integer :: i,j,k, lng, rtid, rt, lnc , rtsizes(3)

    lng = 0; if ( present(ng) ) lng = ng
    lnc = 1; if ( present(nc) ) lnc = nc
    rtsizes = get_regionsizes(ata)
    if(rtsizes(1) < lng .or. &
       rtsizes(2) < lng .or. &
       rtsizes(3) < lng ) then
       !when we fill the boundaries of tiles, we get the values from neighboring tiles
       !if the tilesize < ng, then it requires more than one neighboring tile, so we disallow 
       !creating tiles smaller than ghost cells. There is an exception for the last tiles since
       !they will get their values from the boundaries of the box, that's why we only check 
       !the size of the first mtile in the list
       call tida_error("TILEARRAY_BUILD: region size chosen is smaller than number of ghost cells")
    end if

    if (numregions(ata) < 1) then 
       call tida_error("TILEARRAY_BUILD: there is no region")
    end if

    ta%dim = get_dim(ata)
    ta%ng  = lng
    ta%nc  = lnc
    ta%numregions = numregions(ata)
    ta%regionextent = regionextent(ata)
    ta%bigtile = get_bigtile(ata)
    ta%regionsizes = get_regionsizes(ata)

    allocate( ta%regions(numregions(ata,1), numregions(ata,2), numregions(ata,3)) )
    allocate( ta%region_xyzids(ta%numregions,ta%dim) )
    
    do rt = 1, ta%numregions
       tl = get_regiontile(ata,rt)
       i = get_id(tl,1)
       j = get_id(tl,2)
       k = get_id(tl,3)
       ta%regions(i,j,k) = region_build(lwb(tl), upb(tl), ta%dim, ta%nc, lng)
       ta%region_xyzids(rt,1) = i
       ta%region_xyzids(rt,2) = j
       ta%region_xyzids(rt,3) = k
       call set_id(ta%regions(i,j,k), get_id(tl))
       call set_id(ta%regions(i,j,k), i, j, k)
    enddo      

  end function tilearray_build

  subroutine tilearray_fill_boundary(ta, ata)
    type(tilearray), intent(inout) :: ta
    type(abstilearray), intent(in) :: ata
    integer :: ng, nc, c_tid(3), p_tid(3), lo(3), hi(3), plo(3), phi(3), k , c, g
    type(tileitr) :: ti
    type(tile) :: tl, p_tl
    double precision, pointer :: dp_cr(:,:,:,:), dp_pr(:,:,:,:)

    ng = nghost(ta)
    nc = ncomp(ta)

    ti = btileitr_build(ata,1)
    do while( next_tile(ti) )
       tl = get_tile(ti)
       dp_cr => dataptr(ta,tl)
       lo = lwb(tl)
       hi = upb(tl)
       c_tid(1) = get_id(tl,1);
       c_tid(2) = get_id(tl,2);
       c_tid(3) = get_id(tl,3);
       p_tid = c_tid

       if ( (c_tid(1)-1).lt.1 ) then
          p_tid(1) = extent(ata,1)
       else
          p_tid(1) = c_tid(1)-1
       end if
       p_tl = get_tile(ata, p_tid(1),p_tid(2),p_tid(3))
       dp_pr => dataptr(ta, p_tl)
       plo = lwb(p_tl)
       phi = upb(p_tl)

       do c = 1, nc
          do k = lo(3),hi(3)
             dp_cr(lo(1)-ng:lo(1)-1,lo(2):hi(2),k,c) = dp_pr(phi(1)-ng+1:phi(1),plo(2):phi(2),k,c)
          end do
       end do

       do c = 1, nc
          do k = lo(3),hi(3)
             dp_pr(phi(1)+1:phi(1)+ng,plo(2):phi(2),k,c) = dp_cr(lo(1):lo(1)+ng-1,lo(2):hi(2),k,c)
          end do
       end do

    end do
    call destroy(ti)
!$OMP BARRIER

    ti = btileitr_build(ata,2)
    do while( next_tile(ti) )
       tl = get_tile(ti)
       dp_cr => dataptr(ta,tl)
       lo = lwb(tl)
       hi = upb(tl)
       c_tid(1) = get_id(tl,1);
       c_tid(2) = get_id(tl,2);
       c_tid(3) = get_id(tl,3);
       p_tid = c_tid

       if ( (c_tid(2)-1).lt.1 ) then
          p_tid(2) = extent(ata,2)
       else
          p_tid(2) = c_tid(2)-1
       end if
       p_tl = get_tile(ata, p_tid(1),p_tid(2),p_tid(3))
       dp_pr => dataptr(ta, p_tl)
       plo = lwb(p_tl)
       phi = upb(p_tl)

       do c = 1, nc
          do k = lo(3),hi(3)
             dp_cr(lo(1):hi(1),lo(2)-ng:lo(2)-1,k,c) = dp_pr(plo(1):phi(1),phi(2)-ng+1:phi(2),k,c)
          end do
       end do

       do c = 1, nc
          do k = lo(3),hi(3)
             dp_pr(plo(1):phi(1),phi(2)+1:phi(2)+ng,k,c) = dp_cr(lo(1):hi(1),lo(2):lo(2)+ng-1,k,c)
          end do
       end do

    end do
    call destroy(ti)
!$OMP BARRIER

    ti = btileitr_build(ata,3)
    do while( next_tile(ti) )
       tl = get_tile(ti)
       dp_cr => dataptr(ta,tl)
       lo = lwb(tl)
       hi = upb(tl)
       c_tid(1) = get_id(tl,1);
       c_tid(2) = get_id(tl,2);
       c_tid(3) = get_id(tl,3);
       p_tid = c_tid

       if ( (c_tid(3)-1).lt.1 ) then
          p_tid(3) = extent(ata,3)
       else
          p_tid(3) = c_tid(3)-1
       end if
       p_tl = get_tile(ata, p_tid(1),p_tid(2),p_tid(3))
       dp_pr => dataptr(ta, p_tl)
       plo = lwb(p_tl)
       phi = upb(p_tl)

       do c = 1, nc
          do g = ng,1,-1
             dp_cr(lo(1):hi(1),lo(2):hi(2),lo(3)-g,c) = dp_pr(plo(1):phi(1),plo(2):phi(2),phi(3)-g+1,c)
          end do
       end do

       do c = 1, nc
          do g = 1,ng
             dp_pr(plo(1):phi(1),plo(2):phi(2),phi(3)+g,c) = dp_cr(lo(1):hi(1),lo(2):hi(2),lo(3)+g-1,c)
          end do
       end do

    end do
    call destroy(ti)

  end subroutine tilearray_fill_boundary

end module tilearray_module
