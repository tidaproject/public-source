!! Author: Didem Unat 
!! email : dunat@lbl.gov
!!
!! This module is created to define a metadata for a tile 
!! Date: May 2013
!! Date: Nov 2013: renamed modules 
!!
!! Modified by: M Nufail Farooqi
!! Nov 2015
!! mtile is renamed to tile and is stands for a logical tile

module tile_module

  use tida_const
  use tida_error_module

  implicit none

  !! tiles consist of
  !!   - an id indicates its ID in single dimensional space (flattened abstarc tile array)
  !!   - an regionid indicates the ID of the region inside which the tile exist.
  !!   - A low(dim) and high(dim) indicates the start and end of a region in array
  !! Initially, each tile is completely empty, with a low end at positive
  !! infinity and a hi end at negative infinity.  Almost all tile operations
  !! are ill-defined on default tiles. I won't say which ones are not!

  type tile
     integer :: dim
     integer :: id  = 0
     integer :: idxyz(3) = 0
     integer :: regionid  = 0
     integer :: lo(MAX_SPACEDIM) =  Huge(1)
     integer :: hi(MAX_SPACEDIM) = -Huge(1)
  end type tile


  !! Builds a tile
  interface tile_build
     module procedure tile_build_2
     module procedure tile_build_1
  end interface

  interface get_dim
     module procedure tile_get_dim
  end interface

  !! Returns the upper bound of a tile, or of a tile in
  !! a given direction
  interface upb
     module procedure tile_upb
     module procedure tile_upb_d
  end interface
  !! Sets the upper bound of a tile, or sets the upper
  !! bound of a tile in a given direction
  interface set_upb
     module procedure tile_set_upb
     module procedure tile_set_upb_d
  end interface set_upb

  !! Returns the lower bound of a tile, or of a tile in
  !! a given direction
  interface lwb
     module procedure tile_lwb
     module procedure tile_lwb_d
  end interface

  !! Sets the lower bound of a tile, or sets the lower
  !! bound of a tile in a given direction
  interface set_lwb
     module procedure tile_set_lwb
     module procedure tile_set_lwb_d
  end interface set_lwb

  !! Creates a new tile by growing the given tile's lo and hi by ng
  interface grow
     module procedure tile_grow
  end interface

  !! Returns the size, or extent of a tile
  !! in a given direction d.
  interface extent
     module procedure tile_extent
     module procedure tile_extent_d
  end interface

  interface set_id
     module procedure tile_set_id
     module procedure tile_set_idxyz
  end interface

  interface get_id
     module procedure tile_get_id 
     module procedure tile_get_idxyz 
  end interface

  interface set_regionid
     module procedure tile_set_regionid
  end interface

  interface get_regionid
     module procedure tile_get_regionid 
  end interface

  interface destroy
     module procedure tile_destroy
  end interface

  interface volume
     module procedure tile_volume
  end interface

  interface print
     module procedure tile_print
     module procedure tile_print_ijk
  end interface

contains

  subroutine tile_destroy(tl)
    type(tile), intent(inout) ::tl    
    tl%id = 0
    tl%idxyz = 0
    tl%regionid = 0
    tl%lo(1:tl%dim) =  Huge(1)
    tl%hi(1:tl%dim) = -Huge(1)
  end subroutine tile_destroy

  subroutine tile_set_id(tl, id)
    integer, intent(in) :: id
    type (tile), intent(inout) :: tl
    if ( id < 1    ) call tida_error("TILE_SET_ID: id < 1")
    tl%id = id
  end subroutine tile_set_id

  pure function tile_get_dim(tl) result(r)
    type(tile), intent(in) :: tl
    integer :: r
    r = tl%dim
  end function tile_get_dim

  subroutine tile_set_idxyz(tl, i,j,k)
    integer, intent(in) :: i,j
    integer, intent(in), optional :: k
    type (tile), intent(inout) :: tl

    if ( i < 1    ) call tida_error("TILE_SET_ID: idx < 1")
    if ( j < 1    ) call tida_error("TILE_SET_ID: idy < 1")
    tl%idxyz(1) = i
    tl%idxyz(2) = j
    if ( present(k) .and. k < 1    ) call tida_error("TILE_SET_ID: idz < 1")
    tl%idxyz(3) = k
  end subroutine tile_set_idxyz

  subroutine tile_set_regionid(tl, regionid)
    integer, intent(in) :: regionid
    type (tile), intent(inout) :: tl
    if ( regionid < 1    ) call tida_error("TILE_SET_REGIONID: regionid < 1")
    tl%regionid = regionid
  end subroutine tile_set_regionid

  pure function tile_get_id(tl) result(r)
    type (tile), intent(in) :: tl
    integer :: r
    r = tl%id 
  end function tile_get_id

  function tile_get_idxyz(tl, d) result(r) 
    type (tile), intent(in) :: tl
    integer, intent(in) :: d
    integer :: r
    r = 0
    if ( d < 1 .or. d > tl%dim  ) call tida_error("TILE_GET_IDXYZ: dim")
    r = tl%idxyz(d) 
  
  end function tile_get_idxyz

  pure function tile_get_regionid(tl) result(r)
    type (tile), intent(in) :: tl
    integer :: r
    r = tl%regionid 
  end function tile_get_regionid

  !! Builds a tile given a low and high end of index space.  It is an error
  !! for the sizes of _lo_ and _hi_ to be unequal or for the size of _lo_
  !! to exceed MAX_SPACEDIM.
  !! By default the tile is in region 1
  function tile_build_2(lo, hi) result(tl)
    type(tile) :: tl
    integer, intent(in) :: lo(:), hi(:)
    if ( size(lo) /= size(hi)    ) call tida_error("TILE_BUILD_2: lo /= hi")
    if ( size(lo) > MAX_SPACEDIM ) call tida_error("TILE_BUILD_2: size(lo,hi) > MAX_SPACEDIM")
    tl%dim = size(lo)
    tl%id = 1
    tl%idxyz = 1
    tl%regionid = 1
    tl%lo(1:tl%dim) = lo
    tl%hi(1:tl%dim) = hi

  end function tile_build_2

  !! Builds a unit tile at position _lo_.  It is an error for size of _lo_
  !! to exceed MAX_SPACEDIM.
  function tile_build_1(lo) result(tl)
    type(tile) :: tl
    integer, intent(in) :: lo(:)
    tl = tile_build_2(lo, lo)
  end function tile_build_1

  pure function tile_extent(tl) result(r)
    type (tile), intent(in) :: tl
    integer, dimension(size(tl%lo)) :: r
    integer :: dim
    dim = size(tl%lo)
    r = tl%hi(1:dim) - tl%lo(1:dim) + 1
  end function tile_extent

  pure function tile_extent_d(tl, dim) result(r)
    type (tile), intent(in) :: tl
    integer, intent(in) :: dim
    integer :: r

    r = tl%hi(dim)-tl%lo(dim)+1
  end function tile_extent_d

  pure function tile_grow(tl, ri) result(r)
    type(tile), intent(in) :: tl
    integer, intent(in) :: ri
    type(tile) :: r

    r = tl 
    r%lo(1:r%dim) = tl%lo(1:tl%dim) - ri
    r%hi(1:r%dim) = tl%hi(1:tl%dim) + ri
  end function tile_grow

  pure function tile_lwb(tl) result(r)
    type (tile), intent(in) :: tl
    integer, dimension(size(tl%lo)) :: r
    integer :: dim
    dim = size(tl%lo)
    r = Huge(1)
    r(1:dim) = tl%lo(1:dim)
  end function tile_lwb

  pure function tile_lwb_d(tl, dim) result(r)
    type (tile), intent(in) :: tl
    integer, intent(in) :: dim
    integer :: r
    r = tl%lo(dim)
  end function tile_lwb_d

  subroutine tile_set_lwb(tl, iv)
    integer, intent(in), dimension(:) :: iv
    type (tile), intent(inout) :: tl
    tl%lo = iv
  end subroutine tile_set_lwb

  subroutine tile_set_lwb_d(tl, dim, v)
    integer, intent(in) :: v
    integer, intent(in) :: dim
    type (tile), intent(inout) :: tl
    tl%lo(dim) = v
  end subroutine tile_set_lwb_d

  pure function tile_upb(tl) result(r)
    type (tile), intent(in) :: tl
    integer, dimension(size(tl%hi)) :: r
    integer :: dim
    dim = size(tl%hi)
    r = -Huge(1)
    r(1:dim) = tl%hi(1:dim)
  end function tile_upb

  pure function tile_upb_d(tl, dim) result(r)
    type (tile), intent(in) :: tl
    integer, intent(in) :: dim
    integer :: r

    r = tl%hi(dim)
  end function tile_upb_d

  subroutine tile_set_upb(tl, iv)
    integer, intent(in), dimension(:) :: iv
    type (tile), intent(inout) :: tl
    tl%hi = iv
  end subroutine tile_set_upb

  subroutine tile_set_upb_d(tl, dim, v)
    integer, intent(in) :: v
    integer, intent(in) :: dim
    type (tile), intent(inout) :: tl
    tl%hi(dim) = v
  end subroutine tile_set_upb_d

  function tile_volume(tl) result(r)
    type(tile), intent(in) :: tl
    integer :: r, i, l, dim
    dim = size(tl%lo)
    r = 1
    do i = 1, dim
       l = (tl%hi(i)-tl%lo(i)+1)
       if ( r .le. Huge(r) / l ) then
          r = r*l
       else
          print*,'BAD TILE'
          call tida_error('tile_volume(): overflow')
       end if
    end do
  end function tile_volume
  
  subroutine tile_print(tl)
    type(tile), intent(in) :: tl
    integer :: d

    print*, 'Id ', tile_get_id(tl)
    do d = 1, size(tl%lo)
       print*, "Idxyz dim=", d, tile_get_idxyz(tl,d)
    end do
    print*, 'Regionid ', tile_get_regionid(tl)
    print*, 'Lower ', lwb(tl)
    print*, 'Upper ', upb(tl)

  end subroutine tile_print

  subroutine tile_print_ijk(tl, i, j, k)
    type(tile), intent(in) :: tl
    integer, intent(in) :: i,j,k
    integer:: d
    
    if( tile_get_idxyz(tl,1) == i) then
     if( tile_get_idxyz(tl,2) == j) then
        if( tile_get_idxyz(tl,3) == k) then    
           print*, "Tile Id ", i,j,k
           print*, 'Lower  ', lwb(tl)
           print*, 'Upper  ', upb(tl)
        end if
     end if
  end if

  end subroutine tile_print_ijk

end module tile_module








