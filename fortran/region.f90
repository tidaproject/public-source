!! Didem Unat
!! email : dunat@lbl.gov
!! May 2013
!!
!! A regional tile holds the data for a region
!! REGION stands for Regional Tile
!! Modified by: M Nufail Farooqi
!! Nov 2015

module region_module

  use tida_types
  use tida_const
  use tile_module

  implicit none

  !! A region consists of
  !! DIM:    Dimension
  !  TL:     The tile in index space for which this REGION is defined
  !! PTL:    The physical tile for the Region that also inculdes the ghosts
  !! NG:     Number of ghost cells
  
  !! All Arrays are '4' dimensional, conventially, (NX,NY,NZ) in size and NC is the component.
  !! NY = 1, NZ = 1, when DIM =1, NZ = 1, when DIM = 2.

  type region
     private
     integer    :: dim = 0
     integer    :: nc=1
     integer    :: ng=0
     type(tile) :: tl
     real(kind = dp_t), pointer, dimension(:,:,:,:) :: data => Null()
  end type region

  !! Returns the Dimension  of the REGION
  interface get_dim
     module procedure region_dim
  end interface

  !! Creates a regional tile
  interface region_build
     module procedure region_build
  end interface

  !! Destroys the regional tile
  interface destroy
     module procedure region_destroy
  end interface
  
  interface lwb
     module procedure region_lwb
  end interface

  interface upb 
     module procedure region_upb 
  end interface

  interface plwb
     module procedure region_plwb
  end interface

  interface pupb 
     module procedure region_pupb 
  end interface

  interface tida_sum 
     module procedure region_sum
  end interface

  interface print
     module procedure region_print
  end interface

  interface set_id
     module procedure region_set_id
     module procedure region_set_idxyz
  end interface

  interface get_id
     module procedure region_get_id 
     module procedure region_get_idxyz 
  end interface

  !! Returns whether the REGION has been built, this is different from
  !! whether the underlying pointer has been allocated since on parallel
  !! systems the pointer will not be allocated if the LAYOUT says it
  !! lives on another processor
  interface built_q
     module procedure region_built_q
  end interface

  interface dataptr
     module procedure region_dataptr
  end interface

  !! Returns the tile containg the upper and lower bounds of the region
  interface get_tile
     module procedure region_get_tile
  end interface

  !! Returns the tile containg the physical upper and lower bounds of the region
  !! That is equal to lower bound - number of ghosts and updder bound + number of ghosts
  interface get_ptile
     module procedure region_get_ptile
  end interface

  interface volume
     module procedure region_volume
  end interface

contains
  

  subroutine region_set_id(rtl, id)
    integer, intent(in) :: id
    type (region), intent(inout) :: rtl
    if ( id < 1    ) call tida_error("REGION_SET_ID: id < 1")
    call set_id(rtl%tl, id)
    call set_regionid(rtl%tl, id)
  end subroutine region_set_id

  subroutine region_set_idxyz(rtl, i,j,k)
    integer, intent(in) :: i,j
    integer, intent(in), optional :: k
    type (region), intent(inout) :: rtl

    if ( i < 1    ) call tida_error("REGION_SET_ID: idx < 1")
    if ( j < 1    ) call tida_error("REGION_SET_ID: idy < 1")
    if ( present(k) .and. k < 1    ) call tida_error("REGION_SET_ID: idz < 1")
    call set_id(rtl%tl,i,j,k)

  end subroutine region_set_idxyz

  function region_get_id(rtl) result(id)
    integer :: id
    type (region), intent(inout) :: rtl

    id = get_id(rtl%tl)
  end function region_get_id

  function region_get_idxyz(rtl,d) result(id)
    integer :: id
    integer, intent(in) :: d
    type (region), intent(inout) :: rtl

    id = get_id(rtl%tl,d)
  end function region_get_idxyz

  pure function region_lwb(tb) result(r)
    type(region), intent(in) :: tb
    integer :: r(tb%dim)
    r = lwb(tb%tl)
  end function region_lwb

  pure function region_lwb_n(tb,dim) result(r)
    type(region), intent(in) :: tb
    integer, intent(in) :: dim
    integer :: r
    r = lwb(tb%tl,dim)
  end function region_lwb_n

  pure function region_upb(tb) result(r)
    type(region), intent(in) :: tb
    integer :: r(tb%dim)
    r = upb(tb%tl)
  end function region_upb
  pure function region_upb_n(tb,dim) result(r)
    type(region), intent(in) :: tb
    integer, intent(in) :: dim
    integer :: r
    r = upb(tb%tl, dim)
  end function region_upb_n

  pure function region_plwb(tb) result(r)
    type(region), intent(in) :: tb
    integer :: r(tb%dim)
    r = lwb(tb%tl) - tb%ng
  end function region_plwb

  pure function region_pupb(tb) result(r)
    type(region), intent(in) :: tb
    integer :: r(tb%dim)
    r = upb(tb%tl) + tb%ng
  end function region_pupb

  pure function region_get_tile(tb) result(r)
    type(region), intent(in) :: tb
    type(tile) :: r
    r = tb%tl
  end function region_get_tile

  pure function region_get_ptile(tb) result(r)
    type(region), intent(in) :: tb
    type(tile) :: r
    r = grow(tb%tl, tb%ng)
  end function region_get_ptile
  
  pure function region_built_q(tb) result(r)
    type(region), intent(in) :: tb
    logical :: r
    r = tb%dim /= 0
  end function region_built_q

  function region_build(lo, hi, dim, nc, ng) result(r)
    type(region) :: r
    integer, intent(in) :: dim
    integer, intent(in), optional :: nc, ng
    integer, intent(in) :: lo(dim), hi(dim)
    integer :: llo(dim), lhi(dim)
    integer :: lng, lnc, AllocateStatus

    lng = 0; if ( present(ng) ) lng = ng
    lnc = 1; if ( present(nc) ) lnc = nc
    r%dim = dim
    r%nc = lnc
    r%tl = tile_build(lo,hi)
    llo(1:r%dim) = lo(1:r%dim) - lng
    lhi(1:r%dim) = hi(1:r%dim) + lng

    allocate(r%data(llo(1):lhi(1),llo(2):lhi(2),llo(3):lhi(3),lnc), STAT=AllocateStatus)
    if(AllocateStatus /= 0) call tida_error("REGION_BUILD: Not enough memory")
    
  end function region_build

  function region_volume(tb, all) result(r)
    integer(kind=ll_t) :: r
    type(region), intent(in) :: tb
    logical, intent(in), optional :: all
    if ( all ) then
       r = volume(grow(tb%tl,tb%ng))
    else
       r = volume(tb%tl)
    end if
    r = r * tb%nc
  end function region_volume

  subroutine region_destroy(tb)
    type(region), intent(inout) :: tb
    integer :: DeAllocateStatus
    if ( associated(tb%data) ) then
       deallocate(tb%data, STAT=DeAllocateStatus)
       if(DeAllocateStatus /= 0) call tida_error("REGION_DESTROY: DeAllocation Failed")
    end if
    call destroy(tb%tl)
    tb%dim = 0
  end subroutine region_destroy

  pure function region_dim(tb) result(r)
    type(region), intent(in) :: tb
    integer :: r
    r = tb%dim
  end function region_dim

  function region_dataptr(tb) result(r)
    type(region), intent(in) :: tb
    real(kind=dp_t), pointer :: r(:,:,:,:)
    r => tb%data    
  end function region_dataptr

  subroutine region_print(rtl, c)
    type(region), intent(in) :: rtl
    integer, intent(in) :: c
    type(tile) :: tl
    integer :: i,j,k, lo(3), hi(3)

    tl = rtl%tl 
    lo = lwb(tl)
    hi = upb(tl)

    do k=lo(3), hi(3)
       do j=lo(2), hi(2)
          do i=lo(1), hi(1)             
             print*, rtl%data(i,j,k,c)
          end do
       end do
    end do

  end subroutine region_print

  function region_sum(rtl, boundary, c, ng) result(sum)
    type(region), intent(in) :: rtl
!    integer, optional, intent(in) :: ng
    integer, optional, intent(in) :: c, ng
    logical ,optional, intent(in) :: boundary
    type(tile) :: tl
    integer :: i,j,k,m, lo(3), hi(3),plo(3), phi(3)
    real(dp_t):: sum

    sum = 0.d0
    
    if(present(boundary)) then
       if(boundary) then
          if(present(ng)) then
	     tl = rtl%tl 
	     lo = lwb(tl)
	     hi = upb(tl)
	     tl = grow(rtl%tl, rtl%ng)
	     plo = lwb(tl)
	     phi = upb(tl)
	     if ( plo(1)-lo(1) < ng) then
               tl = grow(rtl%tl,ng) 
	     else
	       tl = grow(rtl%tl, rtl%ng)
	     endif
          else
             tl = grow(rtl%tl, rtl%ng)
          end if
       else 
          tl = grow(rtl%tl, rtl%ng)
       end if
    else 
       tl = rtl%tl 
    end if

    lo = lwb(tl)
    hi = upb(tl)

    if(present(c)) then
       do k=lo(3), hi(3)
          do j=lo(2), hi(2)
             do i=lo(1), hi(1)             
                sum = sum + rtl%data(i,j,k,c)
             end do
          end do
       end do
    else
       do m =1, rtl%nc
          do k=lo(3), hi(3)
             do j=lo(2), hi(2)
                do i=lo(1), hi(1)             
                   sum = sum + rtl%data(i,j,k,m)
                end do
             end do
          end do
       end do
    end if
  end function region_sum

end module region_module
