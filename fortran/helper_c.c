
#include <stdio.h>
#include <stdlib.h> //getenv
#include <string.h>
#include <assert.h>

void remove_all_chars(char* str, char c) {

  char *pr = str, *pw = str;
  while (*pr) {
    *pw = *pr++;
    pw += (*pw != c);
  }
  *pw = '\0';
}

void copy_string(char *target, const char *source)
{
  while(*source)
    {
      *target = *source;
      source++;
      target++;
    }
  *target = '\0';
}


void get_env_var(const char* name, char* value, int* len, int* status)
{
  char name_str[255];

  copy_string(name_str, name); 
  remove_all_chars(name_str, ' ');

  value = getenv(name_str);

  if(value!= NULL){
    printf("value %s\n", value);
  }
  else {
    *status = 1;
    printf("value is not set!\n" ); 
  }
}

char *strtok(char *s, const char *delim) ;
char *strdup(const char *s1);

char** str_split(char* a_str, const char a_delim)
{
  char** result    = 0;
  size_t count     = 0;
  char* tmp        = a_str;
  char* last_comma = 0;
  char delim[2];
  delim[0] = a_delim;
  delim[1] = 0;

  /* Count how many elements will be extracted. */
  while (*tmp)
    {
      if (a_delim == *tmp)
        {
	  count++;
	  last_comma = tmp;
        }
      tmp++;
    }

  /* Add space for trailing token. */
  count += last_comma < (a_str + strlen(a_str) - 1);

  /* Add space for terminating null string so caller
     knows where the list of returned strings ends. */
  count++;

  result = malloc(sizeof(char*) * count);

  if (result)
    {
      size_t idx  = 0;
      char* token = strtok(a_str, delim);

      while (token)
        {
	  assert(idx < count);
	  *(result + idx++) = strdup(token);
	  token = strtok(0, delim);
        }
      assert(idx == count - 1);
      *(result + idx) = 0;
    }

  return result;
}

void parse_str_to_intvec(const char* orig, int lenght,  int* value)
{

  char str[255] ;
  //char* strVal; 
  char** tokens;

  copy_string(str, orig);
  remove_all_chars(str, ' ');
  tokens = str_split(str, ',');

  if (tokens)
    {
      char* temp; 
      int i;
      for (i = 0; *(tokens + i); i++)
        {
	  temp = *(tokens + i); 
	  value[i] = atoi(temp); 
	  free(*(tokens + i));
        }
      free(tokens);
    }



  /* strVal = strtok (str,","); */
  /* printf("strval %s\n", strVal); */
  /* while (strVal != NULL && i < 3) */
  /*   { */
  /*     value[i++] = atoi(strVal); */
  /*     strVal = strtok (NULL, ","); */
  /*   } */
  /* free(strVal); */
  /* printf("Leaving parse str \n"); */
}

