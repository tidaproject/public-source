/*
 * TileIter.h
 *
 * Acknowledgment
 * 	Based on code originally provided by BoxLib
 * 	Modified by Burak BASTEM
 *
 */

#ifndef TILEITER_H_
#define TILEITER_H_

#include <omp.h>
#include <iostream>
#include "TileArray.h"

class TileIter {
public:
	TileIter();
	~TileIter();
	/*
	 * Constructs iterator with default logical tile size and chunk size which are
	 * respectively (16, 16, 16) and (number of tiles)/(number of threads).
	 */
	TileIter(TileArray* tArray);

	/*
	 * Constructs iterator with default logical tile size and chunk size which are
	 * respectively (16, 16, 16) and (number of tiles)/(number of threads) if logical
	 * tiling is allowed. If not allowed just constructs iterator with default chunk size.
	 */
	TileIter(TileArray* tArray, bool tiling);

	/*
	 * Constructs iterator with the given logical tile size and default chunk size
	 * which is (number of tiles)/(number of threads).
	 */
	TileIter(TileArray* tArray, Dimension logicalTileSize);

	/*
	 * Constructs iterator with the given logical tile size and chunk size.
	 */
	TileIter(TileArray* tArray, Dimension logicalTileSize, int chunkSize);

	/*
	 * Determines iteration space of the iterator for threads.
	 * Divides only necessary regional tiles to logical tiles that will be in the iteration
	 * space of iterator if logical tiling is allowed.
	 */
	void init(bool tiling, Dimension logicalTileSize, int chunkSize);

	/*
	 * Returns a boolean indicating there is a tile left in the iterator.
	 */
	bool isValid();

	/*
	 * Returns the current tile in the iterator.
	 */
	Tile getTile(TileArray* ta);

	/*
	 * Iterates to next tile in the iterator.
	 */
	void next();

	/*
	 * Resets the iterator for another time step or for something else
	 */
	void reset();

private:
	int threadID; // ID of the thread in which the iterator is created.
	int numOfThreads; // number of threads.
	int currentIndex; // index of current tile in the tile array of iterator.
	TileArray* tArray; // tile array which contains regional tiles.
	Tile* tilesOfIterator; // tile array which contains tiles that iterator will iterate. It contains logical tiles if logical tiling allowed.
	int sizeOfTilesOfIterator; // size of the tile array of iterator.
};

#endif /* TILEITER_H_ */
