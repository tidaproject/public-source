/*
 * Tile.h
 */

#ifndef TILE_H_
#define TILE_H_

#include "Dimension.h"

class Tile {
public:
	Tile();
	~Tile();

	// tiling with 1D
	//Tile(double* data, Dimension ID, Dimension low, Dimension high, Dimension size);
	//Tile(double* data, Dimension ID, Dimension low, Dimension high, Dimension size, Dimension sizeOfGhostCells);
	//Tile(double* data, Dimension ID, Dimension low, Dimension high, Dimension size, Dimension sizeOfGhostCells, Dimension sizeWithGhostCells);

	// tiling with 3D
	Tile(double*** data, Dimension ID, Dimension low, Dimension high, Dimension size);
	Tile(double*** data, Dimension ID, Dimension low, Dimension high, Dimension size, Dimension sizeOfGhostCells);
	Tile(double*** data, Dimension ID, Dimension low, Dimension high, Dimension size, Dimension sizeOfGhostCells, Dimension sizeWithGhostCells);

	// tiling with 1D
	//double* getData();

	// tiling with 3D
	double*** getData();

	Dimension getID();
	Dimension getLow();
	Dimension getHigh();
	Dimension getSize();
	Dimension getSizeOfGhostCells();
	Dimension getSizeWithGhostCells();

	// tiling with 1D
	//double& dataAt(int depth, int height, int width);
	//double& operator()(int depth, int height, int width);
private:
	// tiling with 1D
	//double* data;

	// tiling with 3D
	double*** data;

	Dimension ID;
	Dimension low;
	Dimension high;
	Dimension size;
	Dimension sizeOfGhostCells;
	Dimension sizeWithGhostCells;
};

#endif /* TILE_H_ */
