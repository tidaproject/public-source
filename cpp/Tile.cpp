/*
 * Tile.cpp
 */

#include "Tile.h"

Tile::Tile() {

}

Tile::~Tile() {

}

// tiling with 1D
/*Tile::Tile(double* data, Dimension ID, Dimension low, Dimension high, Dimension size) :
		data(data), ID(ID), low(low), high(high), size(size), sizeOfGhostCells(sizeOfGhostCells) {
	sizeOfGhostCells = Dimension(0, 0, 0);
	sizeWithGhostCells = size;
}

Tile::Tile(double *data, Dimension ID, Dimension low, Dimension high, Dimension size, Dimension sizeOfGhostCells) :
	data(data), ID(ID), low(low), high(high), size(size), sizeOfGhostCells(sizeOfGhostCells) {
	sizeWithGhostCells = size + (sizeOfGhostCells * 2);
}

Tile::Tile(double *data, Dimension ID, Dimension low, Dimension high, Dimension size, Dimension sizeOfGhostCells, Dimension sizeWithGhostCells) :
	data(data), ID(ID), low(low), high(high), size(size), sizeOfGhostCells(sizeOfGhostCells), sizeWithGhostCells(sizeWithGhostCells) {

}*/

// tiling with 3D
Tile::Tile(double*** data, Dimension ID, Dimension low, Dimension high, Dimension size) :
				data(data), ID(ID), low(low), high(high), size(size) {
	sizeOfGhostCells = Dimension(0, 0, 0);
	sizeWithGhostCells = size;
}

Tile::Tile(double ***data, Dimension ID, Dimension low, Dimension high, Dimension size, Dimension sizeOfGhostCells) :
				data(data), ID(ID), low(low), high(high), size(size), sizeOfGhostCells(sizeOfGhostCells) {
	sizeWithGhostCells = size + (sizeOfGhostCells * 2);
}

Tile::Tile(double*** data, Dimension ID, Dimension low, Dimension high, Dimension size, Dimension sizeOfGhostCells, Dimension sizeWithGhostCells) :
		data(data), ID(ID), low(low), high(high), size(size), sizeOfGhostCells(sizeOfGhostCells), sizeWithGhostCells(sizeWithGhostCells) {

}

Dimension Tile::getID() {
	return ID;
}

// tiling with 1D
/*double* Tile::getData() {
	return data;
}*/

// tiling with 3D
double*** Tile::getData() {
	return data;
}

Dimension Tile::getLow() {
	return low;
}

Dimension Tile::getHigh() {
	return high;
}

Dimension Tile::getSize() {
	return size;
}

Dimension Tile::getSizeOfGhostCells() {
	return sizeOfGhostCells;
}

Dimension Tile::getSizeWithGhostCells() {
	return sizeWithGhostCells;
}

// tiling with 1D
/*double& Tile::dataAt(int depth, int height, int width) {
	return data[(depth * sizeWithGhostCells(1) + height) * sizeWithGhostCells(2) + width];
}

 double& Tile::operator()(int depth, int height, int width)  {
	 return data[(depth * sizeWithGhostCells(1) + height) * sizeWithGhostCells(2) + width];
 }*/
