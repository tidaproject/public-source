/*
 * Dimension.h
 */

#ifndef DIMENSION_H_
#define DIMENSION_H_

class Dimension {
public:
	Dimension();
	~Dimension();
	Dimension(int depth, int height, int width);
	int& operator()(int dim);
	Dimension operator+(Dimension dim);
	Dimension operator+(int value);
	Dimension operator-(Dimension dim);
	Dimension operator-(int value);
	Dimension operator/(Dimension dim);
	Dimension operator/(int value);
	Dimension operator*(Dimension dim);
	Dimension operator*(int value);
	Dimension operator%(Dimension dim);
	Dimension operator%(int value);
	bool operator==(Dimension dim);
private:
	int dimensions[3];
};

#endif /* DIMENSION_H_ */

