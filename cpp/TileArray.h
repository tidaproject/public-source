/*
 * TileArray.h
 */

#ifndef TILEARRAY_H_
#define TILEARRAY_H_

#include "Tile.h"

class TileArray {
public:
	TileArray();
	~TileArray();

	/*
	 * Constructs tile array by dividing data into regional tiles and specifying
	 * metadata for each tile.
	 */
	TileArray(Dimension sizeOfData, Dimension sizeOfTiles, Dimension sizeOfGhostCells);
	Dimension getSizeOfData();
	Dimension getSizeOfTiles();
	Dimension getNumOfTiles();
	int getTotalNumOfTiles();
	Tile* getTiles();

	/*
	 * Sets given value to data of tiles.
	 */
	void setValue(double value);

	// tiling with 1D
	/*
	 * Sets given data to data of tiles.
	 */
	//void setValue(double *data);

	// tiling with 3D
	/*
	 * Sets given data to data of tiles.
	 */
	void setValue(double ***data);

	/*
	 * Sets given value to data in the boundaries.(not ghost cells of tiles)
	 */
	void initBoundary(double value);

	/*
	 * Updates ghost cells of tiles.
	 */
	void fillTileBoundaries();

	Tile& getTile(int index);
	Tile& getTile(Dimension dim);
	Tile& getTile(int depth, int height, int width);

	// tiling with 3D
	double*** alloc3D(Dimension dim);

private:
	Dimension sizeOfData; // size of data that is wanted to be regionally tiled.
	Dimension sizeOfTiles; // size of regional tiles
	Dimension numOfTiles; // number of tiles as a dimension
	int totalNumOfTiles; // number of tiles in total
	Tile* tiles; // array that contains regional tiles
};

#endif /* TILEARRAY_H_ */
