/*
 * TileIter.cpp
 *
 * Acknowledgment
 * 	Based on code originally provided by BoxLib
 * 	Modified by Burak BASTEM
 *
 */

#include "TileIter.h"

TileIter::TileIter() {
	// TODO Auto-generated constructor stub
}

TileIter::~TileIter() {
	// TODO Auto-generated destructor stub
}

/*
 * Constructs iterator with default logical tile size and chunk size which are
 * respectively (16, 16, 16) and (number of tiles)/(number of threads).
 */
TileIter::TileIter(TileArray* tArray) : tArray(tArray) {
	// initialization is made with init method.
	// true is given as tiling indicating logical tiling will be applied, (16, 16, 16)
	// is given as the default logical tile size and -1 is given as chunk size
	// indicating default chunk size will be applied.
	init(true, Dimension(16, 16, 16), -1);
}

/*
 * Constructs iterator with default logical tile size and chunk size which are
 * respectively (16, 16, 16) and (number of tiles)/(number of threads) if logical
 * tiling is allowed. If not allowed just constructs iterator with default chunk size.
 */
TileIter::TileIter(TileArray* tArray, bool tiling) : tArray(tArray) {
	// initialization is made with init method.
	// (16, 16, 16) is given as the default logical tile size and -1 is given as chunk
	// size indicating default chunk size will be applied.
	init(tiling, Dimension(16, 16, 16), -1);
}

/*
 * Constructs iterator with the given logical tile size and default chunk size
 * which is (number of tiles)/(number of threads).
 */
TileIter::TileIter(TileArray* tArray, Dimension logicalTileSize) : tArray(tArray) {
	// initialization is made with init method.
	// true is given as tiling indicating logical tiling will be applied and -1 is
	// given as chunk size indicating default chunk size will be applied.
	if(tArray->getSizeOfTiles() == logicalTileSize) init(false, logicalTileSize, -1);
	else init(true, logicalTileSize, -1);
}

/*
 * Constructs iterator with the given logical tile size and chunk size.
 */
TileIter::TileIter(TileArray* tArray, Dimension logicalTileSize, int chunkSize) : tArray(tArray) {
	// initialization is made with init method.
	// true is given as tiling indicating logical tiling will be applied
	if(tArray->getSizeOfTiles() == logicalTileSize) init(false, logicalTileSize, chunkSize);
	else init(true, logicalTileSize, chunkSize);
}

/*
 * Determines iteration space of the iterator for threads.
 * Divides only necessary regional tiles to logical tiles that will be in the iteration
 * space of iterator if logical tiling is allowed.
 */
void TileIter::init(bool tiling, Dimension logicalTileSize, int chunkSize) {
	//threadID = 0;
	//numOfThreads = 1;
	threadID = omp_get_thread_num();
	numOfThreads = omp_get_num_threads();
	currentIndex = 0;
	sizeOfTilesOfIterator = 0;

	// Calculating how many tiles will be in total starts here.
	int n_tot_tiles = 0; // number of tiles in total
	Dimension* nt_in_regional_tiles; // array of dimensions which indicate how many logical tiles are in each dimension of regional tile.
	if(tiling){ // if logical tiling is allowed.
		nt_in_regional_tiles = new Dimension[tArray->getTotalNumOfTiles()];
		for (int i = 0; i < tArray->getTotalNumOfTiles(); i++) {
			Tile& t = tArray->getTile(i);
			int ntiles = 1; // number of tiles in a regional tile
			Dimension nt; // number of tiles in each dimension of a regional tile
			for (int d = 0; d < 3; d++){ // d: dimension, number of tiles in each dimension is calculated
				nt(d) = std::max(t.getSize()(d)/logicalTileSize(d), 1); // if logical dimension size is bigger than regional dimension size, no logical tiling is applied.
				ntiles *= nt(d);
			}
			nt_in_regional_tiles[i] = nt;
			n_tot_tiles += ntiles;
		}
	} else { // if logical tiling is not allowed, number of tiles in total will be equal to number of regional tiles.
		n_tot_tiles = tArray->getTotalNumOfTiles();
	}
	// Calculating how many tiles will be in total finishes here.

	// Calculating iteration space of the iterator for the thread starts here.
	int tlo, thi, sz;
	if (chunkSize <= 0) { // Number of tiles in the iterator is calculated with default chunk size which is (number of tiles)/(number of threads).
		// figure out the tile no range, tlo and thi for this thread
		if (n_tot_tiles < numOfThreads) { // There are more threads than tiles.
			if (threadID < n_tot_tiles) {
				tlo = thi = threadID;
			} else { // inaccessible area; something like thread ID assignment must be wrong.
				return;
			}
		} else { // There are more tiles than threads.
			int tiles_per_thread = n_tot_tiles/numOfThreads;
			int nleft = n_tot_tiles - tiles_per_thread*numOfThreads;
			if (threadID < nleft) {
				tlo = threadID*(tiles_per_thread+1);
				thi = tlo + tiles_per_thread;
			} else {
				tlo = threadID*tiles_per_thread + nleft;
				thi = tlo + tiles_per_thread - 1;
			}
		}
		sz = thi-tlo+1;
	} else { // Number of tiles in the iterator is calculated with the given chunk size.
		tlo = -1;
		thi = n_tot_tiles;
		int nchunks = (n_tot_tiles+chunkSize-1) / chunkSize;
		int chunks_per_thread = nchunks/numOfThreads;
		int nleft = nchunks-chunks_per_thread*numOfThreads;
		if (threadID < nleft) chunks_per_thread++;
		sz = chunks_per_thread*chunkSize;
	}
	sizeOfTilesOfIterator = sz;
	tilesOfIterator = new Tile[sz];
	// Calculating iteration space of the iterator for the thread finishes here.

	// Specifying and placing logical tiles starts here.
	int it = 0;
	int indexOfTile = 0;
	for (int i = 0; i < tArray->getTotalNumOfTiles(); i++) {
		Tile& tile = tArray->getTile(i);
		if(tiling) { // Logical tiling is allowed.
			int ntiles = 1;
			for (int d=0; d<3; d++) { // d: dimension
				ntiles *= nt_in_regional_tiles[i](d);
			}
			if (it+ntiles-1 < tlo) {
				it += ntiles;
				continue;
			}
			Dimension tsize, nleft;
			for (int d=0; d<3; d++) { // d: dimension
				int ncells = tile.getSize()(d);
				tsize(d) = ncells/nt_in_regional_tiles[i](d);
				nleft(d) = ncells - nt_in_regional_tiles[i](d)*tsize(d);
			}
			Dimension small = Dimension(0, 0, 0); // low of logical tile
			Dimension big = Dimension(0, 0, 0); // high of logical tile
			Dimension ijk = Dimension(0, 0, 0); // ith, jth and kth logical tile in the current regional tile
			// note that the initial values are all zero.
			ijk(2) = -1;
			for (int t=0; t<ntiles; t++) {
				for (int d=2; d>=0; d--) { // d: dimension
					if (ijk(d)<nt_in_regional_tiles[i](d)-1) {
						ijk(d)++;
						break;
					} else {
						ijk(d) = 0;
					}
				}
				if ( (chunkSize <=0 && it >= tlo) ||
						(chunkSize > 0 && (it/chunkSize)%numOfThreads == threadID) ) {
					for (int d=0; d<3; d++) { // d: dimension
						if (ijk(d) < nleft(d)) {
							small(d) = ijk(d)*(tsize(d)+1) + tile.getSizeOfGhostCells()(d);
							big(d) = small(d) + tsize(d);
						} else {
							small(d) = ijk(d)*tsize(d) + nleft(d) + tile.getSizeOfGhostCells()(d);
							big(d) = small(d) + tsize(d) - 1;
						}
					}
					Tile ltile = Tile(tile.getData(), tile.getID(), small, big, tile.getSize(), tile.getSizeOfGhostCells(), tile.getSizeWithGhostCells());
					tilesOfIterator[indexOfTile] = ltile;
					indexOfTile++;
				}
				it++;
				if (it > thi) return;
			}
		} else { // Logical tiling is not allowed.
			if (it < tlo) {
				it++;
				continue;
			}
			if ( (chunkSize <=0 && it >= tlo) ||
					(chunkSize > 0 && (it/chunkSize)%numOfThreads == threadID) ) {
				tilesOfIterator[indexOfTile] = tile;
				indexOfTile++;
			}
			it++;
			if (it > thi) return;
		}
	}
	// Specifying and placing logical tiles starts here.
}

/*
 * Returns a boolean indicating there is a tile left in the iterator.
 */
bool TileIter::isValid() {
	if(currentIndex < sizeOfTilesOfIterator) return true;
	else return false;
}

/*
 * Returns the current tile in the iterator.
 */
Tile TileIter::getTile(TileArray* ta) {
	Tile& t = tilesOfIterator[currentIndex];

	// tiling with 1D
	//double* dataPtr = ta->getTile(t.getID()).getData();

	// tiling with 3D
	double*** dataPtr = ta->getTile(t.getID()).getData();
	Tile logicalTile = Tile(dataPtr, t.getID(), t.getLow(), t.getHigh(), t.getSize(), t.getSizeOfGhostCells(), t.getSizeWithGhostCells());
	return logicalTile;
}

/*
 * Iterates to next tile in the iterator.
 */
void TileIter::next() {
	currentIndex++;
}

/*
 * Resets the iterator for another time step or for something else
 */
void TileIter::reset() {
	currentIndex = 0;
}
