/*
 * TileArray.cpp
 */

#include "TileArray.h"
#include <omp.h>

using namespace std;


TileArray::TileArray() {

}

TileArray::~TileArray() {

}

/*
 * Constructs tile array by dividing data into regional tiles and specifying
 * metadata for each tile.
 */
TileArray::TileArray(Dimension sizeOfData, Dimension sizeOfTiles, Dimension sizeOfGhostCells) : sizeOfData(sizeOfData), sizeOfTiles(sizeOfTiles) {
	numOfTiles = sizeOfData / sizeOfTiles; // Calculates the number of tiles on each dimension.

	// If size of data is not evenly divisible by size of tiles, one more tile is added.
	if (sizeOfData(0) % sizeOfTiles(0) != 0) numOfTiles(0)++;
	if (sizeOfData(1) % sizeOfTiles(1) != 0) numOfTiles(1)++;
	if (sizeOfData(2) % sizeOfTiles(2) != 0) numOfTiles(2)++;

	totalNumOfTiles = numOfTiles(0) * numOfTiles(1) * numOfTiles(2); // number of tiles in total is calculated.
	tiles = new Tile[totalNumOfTiles]; // memory for array that will contain all tiles is allocated.

	// Specifying metadata for each tile and allocation of each tile starts here.
#pragma omp for
	for (int i = 0; i < numOfTiles(0); i++) {
		for (int j = 0; j < numOfTiles(1); j++) {
			for (int k = 0; k < numOfTiles(2); k++) {
				Dimension sizeOfTile = sizeOfTiles;

				// If size of data is not evenly divisible by size of tiles, remaining data size is determined.
				if (i == numOfTiles(0) - 1) {
					if (sizeOfData(0) % sizeOfTiles(0) != 0) sizeOfTile(0) = sizeOfData(0) % sizeOfTiles(0);
				}
				if (j == numOfTiles(1) - 1) {
					if (sizeOfData(1) % sizeOfTiles(1) != 0) sizeOfTile(1) = sizeOfData(1) % sizeOfTiles(1);
				}
				if (k == numOfTiles(2) - 1) {
					if (sizeOfData(2) % sizeOfTiles(2) != 0) sizeOfTile(2) = sizeOfData(2) % sizeOfTiles(2);
				}

				Dimension sizeOfTileWithGhostCells = sizeOfTile + (sizeOfGhostCells * 2);
				Dimension IDOfTile = Dimension(i, j, k);
				Dimension lowOfTile = sizeOfGhostCells;
				Dimension highOfTile = (lowOfTile + sizeOfTile) - 1;

				// tiling with 1D
				//double* dataOfTile = new double[sizeOfTileWithGhostCells(0) * sizeOfTileWithGhostCells(1) * sizeOfTileWithGhostCells(2)];

				// tiling with 3D
				double*** dataOfTile = alloc3D(sizeOfTileWithGhostCells);

				Tile currentTile = Tile(dataOfTile, IDOfTile, lowOfTile, highOfTile, sizeOfTile, sizeOfGhostCells, sizeOfTileWithGhostCells);
				tiles[(i * numOfTiles(1) + j) * numOfTiles(2) + k] = currentTile;
			}
		}
	}
	// Specifying metadata for each tile and allocation of each tile finishes here.
}

Dimension TileArray::getSizeOfData() {
	return sizeOfData;
}
Dimension TileArray::getSizeOfTiles() {
	return sizeOfTiles;
}
Dimension TileArray::getNumOfTiles() {
	return numOfTiles;
}
int TileArray::getTotalNumOfTiles() {
	return totalNumOfTiles;
}
Tile* TileArray::getTiles() {
	return tiles;
}

/*
 * Sets given value to data of tiles.
 */
void TileArray::setValue(double value) {
#pragma omp for
	for (int i = 0; i < totalNumOfTiles; i++) {
		Tile& dstTile = getTile(i);
		Dimension low = dstTile.getLow();
		Dimension high = dstTile.getHigh();

		// tiling with 1D
		//double* data = dstTile.getData();

		// tiling with 3D
		double*** data = dstTile.getData();

		for (int a = low(0); a <= high(0); a++) {
			for (int b = low(1); b <= high(1); b++) {
				for (int c = low(2); c <= high(2); c++) {

					// tiling with 1D
					//Dimension sizeWithGhostCells = dstTile.getSizeWithGhostCells();
					//data[(a * sizeWithGhostCells(1) + b) * sizeWithGhostCells(2) + c] = value;

					// tiling with 3D
					data[a][b][c] = value;
				}
			}
		}
	}
}

// tiling with 1D
/*
 * Sets given data to data of tiles.
 */
/*void TileArray::setValue(double* data)
{
#pragma omp for
	for (int i = 0; i < totalNumOfTiles; i++) {
		Tile& dstTile = getTile(i);
		Dimension loDst = dstTile.getLow();
		Dimension hiDst = dstTile.getHigh();

		// tiling with 3D
		//double*** dstData = dstTile.getData();

		Dimension loSrc = dstTile.getID() * sizeOfTiles;
		for (int a = loDst(0), aa = loSrc(0); a <= hiDst(0); a++, aa++) {
			for (int b = loDst(1), bb = loSrc(1); b <= hiDst(1); b++, bb++) {
				for (int c = loDst(2), cc = loSrc(2); c <= hiDst(2); c++, cc++) {

					// tiling with 1D
					dstTile.dataAt(a, b, c) = data[(aa * sizeOfData(1) + bb) * sizeOfData(2) + cc];

					// tiling with 3D
					//dstData[a][b][c] = data[aa][bb][cc];
				}
			}
		}
	}
}*/

// tiling with 3D
/*
 * Sets given data to data of tiles.
 */
void TileArray::setValue(double*** data) {
#pragma omp for
	for (int i = 0; i < totalNumOfTiles; i++) {
		Tile& dstTile = getTile(i);
		Dimension loDst = dstTile.getLow();
		Dimension hiDst = dstTile.getHigh();

		// tiling with 3D
		double*** dstData = dstTile.getData();

		Dimension loSrc = dstTile.getID() * sizeOfTiles;
		for (int a = loDst(0), aa = loSrc(0); a <= hiDst(0); a++, aa++) {
			for (int b = loDst(1), bb = loSrc(1); b <= hiDst(1); b++, bb++) {
				for (int c = loDst(2), cc = loSrc(2); c <= hiDst(2); c++, cc++) {

					// tiling with 1D
					//dstTile.dataAt(a, b, c) = data[(aa * sizeOfData(1) + bb) * sizeOfData(2) + cc];

					// tiling with 3D
					dstData[a][b][c] = data[aa][bb][cc];
				}
			}
		}
	}
}

/*
 * Sets given value to data in the boundaries.(not ghost cells of tiles)
 */
void TileArray::initBoundary(double value) {
#pragma omp for
	for (int i = 0; i < totalNumOfTiles; i++) {
		Tile& dstTile = getTile(i);

		// tiling with 1D
		//double* data = dstTile.getData();

		// tiling with 3D
		double*** data = dstTile.getData();

		Dimension ID = dstTile.getID();
		Dimension low = dstTile.getLow();
		Dimension high = dstTile.getHigh();
		Dimension sizeWithGhostCells = dstTile.getSizeWithGhostCells();
		// for tiles in the front
		if (ID(0) == 0) {
			for (int i = 0; i < low(0); i++) {
				for (int j = 0; j < sizeWithGhostCells(1); j++) {
					for (int k = 0; k < sizeWithGhostCells(2); k++) {

						// tiling with 1D
						//data[(i * sizeWithGhostCells(1) + j) * sizeWithGhostCells(2) + k] = value;

						// tiling with 3D
						data[i][j][k] = value;
					}
				}
			}
		}
		// for tiles on the back
		if (ID(0) == numOfTiles(0) - 1) {
			for (int i = high(0) + 1; i < sizeWithGhostCells(0); i++) {
				for (int j = 0; j < sizeWithGhostCells(1); j++) {
					for (int k = 0; k < sizeWithGhostCells(2); k++) {

						// tiling with 1D
						//data[(i * sizeWithGhostCells(1) + j) * sizeWithGhostCells(2) + k] = value;

						// tiling with 3D
						data[i][j][k] = value;
					}
				}
			}
		}
		// for tiles on the top
		if (ID(1) == 0) {
			for (int i = 0; i < sizeWithGhostCells(0); i++) {
				for (int j = 0; j < low(1); j++) {
					for (int k = 0; k < sizeWithGhostCells(2); k++) {

						// tiling with 1D
						//data[(i * sizeWithGhostCells(1) + j) * sizeWithGhostCells(2) + k] = value;

						// tiling with 3D
						data[i][j][k] = value;
					}
				}
			}
		}
		// for tiles on the bottom
		if (ID(1) == numOfTiles(1) - 1) {
			for (int i = 0; i < sizeWithGhostCells(0); i++) {
				for (int j = high(1) + 1; j < sizeWithGhostCells(1); j++) {
					for (int k = 0; k < sizeWithGhostCells(2); k++) {

						// tiling with 1D
						//data[(i * sizeWithGhostCells(1) + j) * sizeWithGhostCells(2) + k] = value;

						// tiling with 3D
						data[i][j][k] = value;
					}
				}
			}
		}
		// for tiles on the left
		if (ID(2) == 0) {
			for (int i = 0; i < sizeWithGhostCells(0); i++) {
				for (int j = 0; j < sizeWithGhostCells(1); j++) {
					for (int k = 0; k < low(2); k++) {

						// tiling with 1D
						//data[(i * sizeWithGhostCells(1) + j) * sizeWithGhostCells(2) + k] = value;

						// tiling with 3D
						data[i][j][k] = value;
					}
				}
			}
		}
		// for tiles on the right
		if (ID(2) == numOfTiles(2) - 1) {
			for (int i = 0; i < sizeWithGhostCells(0); i++) {
				for (int j = 0; j < sizeWithGhostCells(1); j++) {
					for (int k = high(2) + 1; k < sizeWithGhostCells(2); k++) {

						// tiling with 1D
						//data[(i * sizeWithGhostCells(1) + j) * sizeWithGhostCells(2) + k] = value;

						// tiling with 3D
						data[i][j][k] = value;
					}
				}
			}
		}
	}
}

/*
 * Updates ghost cells of tiles.
 */
void TileArray::fillTileBoundaries() {
#pragma omp for
	for (int t = 0; t < totalNumOfTiles; t++) {
		Tile& dstTile = getTile(t);

		// tiling with 1D
		//double* data = dstTile.getData();

		// tiling with 3D
		double*** data = dstTile.getData();

		Dimension ID = dstTile.getID();
		Dimension low = dstTile.getLow();
		Dimension high = dstTile.getHigh();
		Dimension size = dstTile.getSize();
		Dimension sizeOfGhostCells = dstTile.getSizeOfGhostCells();
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				for (int k = -1; k <= 1; k++) {
					if (i == 0 && j == 0 && k == 0) {
						// nothing to copy
					} else {
						Dimension r = Dimension(i, j, k);
						Dimension srcID = ID + r; // ID of source tile

						// Mirror effect is applied.
						// For instance, ghost cells of tiles in the front
						// is updated with the tiles on the back.
						if (srcID(0) >= numOfTiles(0)) {
							srcID(0) = 0;
						}
						if (srcID(0) < 0) {
							srcID(0) = numOfTiles(0) - 1;
						}
						if (srcID(1) >= numOfTiles(1)) {
							srcID(1) = 0;
						}
						if (srcID(1) < 0) {
							srcID(1) = numOfTiles(1) - 1;
						}
						if (srcID(2) >= numOfTiles(2)) {
							srcID(2) = 0;
						}
						if (srcID(2) < 0) {
							srcID(2) = numOfTiles(2) - 1;
						}

						Tile& srcTile = getTile(srcID);
						Dimension loDst = low + sizeOfGhostCells * ((r - 1) / 2) + ((r + 1) / 2) * size;
						Dimension hiDst = high + ((r - 1) / 2) * size + ((r + 1) / 2) * sizeOfGhostCells;
						// loSrc Bug!
						// If sizes of tiles are not the same, loSrc is not found correctly with the following equation.
						// Operator overloading for % in the dimension class should be revised.
						Dimension loSrc = loDst % srcTile.getSize();

						// tiling with 1D
						//double* srcData = srcTile.getData();
						//Dimension srcSizeWithGhostCells = srcTile.getSizeWithGhostCells();

						// tiling with 3D
						double*** srcData = srcTile.getData();

						for (int a = loDst(0), aa = loSrc(0); a <= hiDst(0); a++, aa++) {
							for (int b = loDst(1), bb = loSrc(1); b <= hiDst(1); b++, bb++) {
								for (int c = loDst(2), cc = loSrc(2); c <= hiDst(2); c++, cc++) {

									// tiling with 1D accessing array data with method
									//dstTile.dataAt(a, b, c) = srcTile.dataAt(aa, bb, cc);

									// tiling with 1D accessing array data with calculation
									//data[(a * sizeWithGhostCells(1) + b) * sizeWithGhostCells(2) + c] = srcData[(aa * srcSizeWithGhostCells(1) + bb) * srcSizeWithGhostCells(2) + cc];

									// tiling with 1D accessing array data with operator overloading
									//dstTile(a, b, c) = srcTile(aa, bb, cc);

									// tiling with 3D
									data[a][b][c] = srcData[aa][bb][cc];
								}
							}
						}
					}
				}
			}
		}
	}
}

Tile& TileArray::getTile(int index) {
	return tiles[index];
}

Tile& TileArray::getTile(Dimension dim) {
	return tiles[(dim(0) * numOfTiles(1) + dim(1)) * numOfTiles(2) + dim(2)];
}

Tile& TileArray::getTile(int depth, int height, int width) {
	return tiles[(depth * numOfTiles(1) + height) * numOfTiles(2) + width];
}

// tiling with 3D
// normal 3D array allocation method
double*** TileArray::alloc3D(Dimension dim) {
	double ***array3D = new double**[dim(0)];
	for (int i = 0; i < dim(0); i++) {
		array3D[i] = new double*[dim(1)];
		for (int j = 0; j < dim(1); j++) {
			array3D[i][j] = new double[dim(2)];
		}
	}
	return array3D;
}

// tiling with 3D
// suggested 3D array allocation method
// dimensions should be corrected
/*double*** TileArray::alloc3D(Dimension dim) {
	double*** temp = (double***)malloc(sizeof(double**)* dim(2) + sizeof(double*)* dim(2) * dim(1) + sizeof(double)* dim(2) * dim(1) * dim(0));
	double* start = (double*)(temp + (dim(2) + 1) * dim(1));

	for (int i = 0; i < dim(2); ++i) {
		temp[i] = (double**)(temp + dim(2)) + i * dim(1);
	}
	for (int i = 0; i < dim(2); ++i) {
		for (int j = 0; j < dim(1); ++j) {
			temp[i][j] = start + (i * dim(1) * dim(0)) + (j * dim(0));
		}
	}
	return temp;
}*/
