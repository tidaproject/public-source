/*
 * Dimension.cpp
 */

#include "Dimension.h"

Dimension::Dimension() {

}

Dimension::~Dimension() {

}

Dimension::Dimension(int depth, int height, int width) {
	dimensions[0] = depth;
	dimensions[1] = height;
	dimensions[2] = width;
}

int& Dimension::operator()(int dim) {
	return dimensions[dim];
}

Dimension Dimension::operator+(Dimension dim) {
	Dimension result;
	result(0) = dimensions[0] + dim(0);
	result(1) = dimensions[1] + dim(1);
	result(2) = dimensions[2] + dim(2);
	return result;
}

Dimension Dimension::operator+(int value) {
	Dimension result;
	result(0) = dimensions[0] + value;
	result(1) = dimensions[1] + value;
	result(2) = dimensions[2] + value;
	return result;
}

Dimension Dimension::operator-(Dimension dim) {
	Dimension result;
	result(0) = dimensions[0] - dim(0);
	result(1) = dimensions[1] - dim(1);
	result(2) = dimensions[2] - dim(2);
	return result;
}

Dimension Dimension::operator-(int value) {
	Dimension result;
	result(0) = dimensions[0] - value;
	result(1) = dimensions[1] - value;
	result(2) = dimensions[2] - value;
	return result;
}

Dimension Dimension::operator/(Dimension dim) {
	Dimension result;
	result(0) = dimensions[0] / dim(0);
	result(1) = dimensions[1] / dim(1);
	result(2) = dimensions[2] / dim(2);
	return result;
}

Dimension Dimension::operator/(int value) {
	Dimension result;
	result(0) = dimensions[0] / value;
	result(1) = dimensions[1] / value;
	result(2) = dimensions[2] / value;
	return result;
}

Dimension Dimension::operator*(Dimension dim) {
	Dimension result;
	result(0) = dimensions[0] * dim(0);
	result(1) = dimensions[1] * dim(1);
	result(2) = dimensions[2] * dim(2);
	return result;
}

Dimension Dimension::operator*(int value) {
	Dimension result;
	result(0) = dimensions[0] * value;
	result(1) = dimensions[1] * value;
	result(2) = dimensions[2] * value;
	return result;
}

Dimension Dimension::operator%(Dimension dim) {
	Dimension result;
	result(0) = dimensions[0] % dim(0);
	result(1) = dimensions[1] % dim(1);
	result(2) = dimensions[2] % dim(2);
	// Following steps are required to maintain
	// correct results for equations used to
	// find loDst and hiDst in fillTileBoundaries
	// method of TileArray class.
	// loSrc Bug!
	if (result(0) == 0) result(0) = dim(0);
	if (result(1) == 0) result(1) = dim(1);
	if (result(2) == 0) result(2) = dim(2);

	return result;
}

Dimension Dimension::operator%(int value) {
	Dimension result;
	result(0) = dimensions[0] % value;
	result(1) = dimensions[1] % value;
	result(2) = dimensions[2] % value;
	// Following steps are required to maintain
	// correct results for equations used to
	// find loDst and hiDst in fillTileBoundaries
	// method of TileArray class.
	// loSrc Bug!
	if (result(0) == 0) result(0) = value;
	if (result(1) == 0) result(1) = value;
	if (result(2) == 0) result(2) = value;

	return result;
}

bool Dimension::operator ==(Dimension dim) {
	if(dimensions[0] == dim(0) && dimensions[1] == dim(1) && dimensions[2] == dim(2)) return true;
	else return false;
}

